# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection

plt.rcParams.update({'font.size':12})

'''
This is for plotting a gradient colored line in a graph

Input:
x,y: coordinates of the points in the graph
z: indicates the color for the linesegments at the points

Output:
plots graph that changes line color with respect to z

Options:
which: title of plot and name of saved file
'''
def JadaColorPlot(x,y,z,which='None'):
	z=z.real
	points = np.array([x, y]).T.reshape(-1, 1, 2)
	segments = np.concatenate([points[:-1], points[1:]], axis=1)
	norm = plt.Normalize(np.min(z), np.max(z))
	lc = LineCollection(segments, cmap='gist_rainbow', norm=norm)
	lc.set_array(z)
	lc.set_linewidth(2)
	fig, ax = plt.subplots()
	line = ax.add_collection(lc)
	cbar = fig.colorbar(line)
	cbar.set_label('Eigenwert')
	if which!='None':
		ax.set_title(which)
	ax.set_xlabel(u'äußere Iteration')
	ax.set_ylabel('$\Vert$ Residuum $\Vert$')
	plt.xlim(np.min(x)-1, np.max(x)+1)
	plt.ylim(np.min(y)*0.1, np.max(y)*10)
	plt.yscale('log')
	plt.savefig(which,dpi=500)
	plt.show()
