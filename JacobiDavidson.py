import numpy as np
import numpy.linalg as la
import scipy.linalg as sl
import scipy.sparse.linalg as ssl
import scipy.sparse as ss
import scipy.io as sio
import tt as tt
import Benchmark.spinchain as spin
import Benchmark.Laplace as laplace
import resaTT.tools as rtt
import matplotlib.pyplot as plt
import time
from Plotting import JadaColorPlot
import GMRES as test
import resatools as rt

'''Berechnet (I-QQ*)(A-r*I)(I-QQ*)x fuer linearOperator'''
def mv(A,x,Q,r):
	z = x.reshape((len(x),1))
	sol = A.dot(z)-r*z
	y = np.dot(rt.conjt(Q),sol)
	sol = sol-Q*y
	return sol

'''
JacobiDavidson Solver

Computes an Eigenpair Q,R of a Matrix A

Input:
A: Matrix
t: Startvector
epsilon: accuracy to achive
maxIter: maximal number of outer Iterations

Output:
(Q,R): (eigenvector,eigenvalue) if not converged: ((0,...,0),0)

Options:
verb: verbosity of output (Default =0)
		0 : runtime of Jada and runtime of parts of Jada
		1 : residual in each outer iteration + output of verb=0
		2 : timinginformation in each outer iteration + output of verb=1
		3+: some orthogonalization-test outputs + output of verb=2
gmres_tol: tolerance for the gmres (if adapt=True we use residuum*gmres_tol as tolerance for gmres)
adapt: set True to use an adapted tolerance in gmres
symm: set to False if problem not symmetric
save: if True adds aditional timeinformations to the Output(Q,R,runtime,gmrestime,outertime)
'''
def JacobiDavidson (A, t, epsilon, maxIter,verb=0,gmres_tol=1e-6,adapt=False,symm=True,save=False,arnoldiIter=0):
	print('JacobiDavidson')
	'''initialisation'''
	start=time.time()
	dim = len(t)
	H = np.zeros((0,0))
	W = np.zeros((dim,0))
	WA = np.zeros((dim,0))
	plotres = np.zeros((0))
	eigs = np.zeros((0))
	times = np.zeros((0))
	k = 0
	gmreszeit=0
	outerzeit=0
	rest=0
	tinit = time.time()-start
	print('inittime: {:5.6f}s'.format(tinit))

	'''outer iteration'''
	for nIter in range(1,maxIter+1):
		'''orthogonalize new searchdirection to searchspace'''
		itstart = time.time()
		v = t - np.dot(W,np.dot(rt.conjt(W),t))
		while la.norm(v) < 1e-16:
			print "random direction needed"
			t = np.random.rand(dim,1)
			v = t - np.dot(W,np.dot(rt.conjt(W),t))
		v = v /la.norm(v)
		v = v - np.dot(W,np.dot(rt.conjt(W),v))
		v = v/la.norm(v)
		
		'''update projection of A onto searchspace'''
		vA = A.dot(v)
		H = np.append(H,np.dot(rt.conjt(W),vA).reshape((nIter-1,1)),axis=1)
		H = np.append(H,np.append(np.dot(rt.conjt(v),WA).reshape((1,nIter-1)),np.dot(rt.conjt(v),vA).reshape((1,1)),axis=1), axis=0)
		W = np.append(W,v,axis=1)
		WA = np.append(WA,vA,axis=1)
		
		'''Compute Schur decomposition H QH = QH RH'''
		if symm:
			#symmetric case
			(RH,QH) = la.eigh(H)
			R = RH[0]
			Q = np.dot(W,QH[:,0].reshape((nIter,1)))
			QA = np.dot(WA,QH[:,0].reshape((nIter,1)))
		else:
			#non-symmetric case
			(RH,QH) = la.eig(H) #eigenvalues not sorted
			ind = np.argmin(RH)
			R = RH[ind]
			Q = np.dot(W,QH[:,ind].reshape((nIter,1)))
			QA = np.dot(WA,QH[:,ind].reshape((nIter,1)))
		eigs = np.append(eigs, R)
		
		'''Compute Residual and check tolerance'''
		res = QA-Q*R
		normres = la.norm(res)
		plotres = np.append(plotres,normres)
		t1 = time.time()-itstart
		outerzeit = outerzeit+t1
		if verb > 0:
			print "Iteration",nIter,":"
			print "res", normres
			print "eig", R
		if verb > 1:
			print('t1: {:5.6f}s'.format(t1))
		if normres < epsilon:
			itend = time.time()
			times = np.append(times,itend-itstart)
			ende = time.time()
			print('outerzeit: {:5.3f}s'.format(outerzeit))
			print('gmreszeit: {:5.3f}s'.format(gmreszeit))
			print('restzeit: {:5.3f}s'.format(rest))
			print('zeit: {:5.3f}s'.format(ende-start))
			#JadaColorPlot(range(1,nIter+1),plotres,eigs,'Jacobi-Davidson')
			#plt.plot(range(1,nIter+1),times[0:nIter])
			#plt.show()
			if save:
				return (Q,R,ende-start,gmreszeit,outerzeit)
			else:
				return (Q,R)

		if nIter <= arnoldiIter:
			if verb>0:
				print "Iteration",nItern,":","generating initial subspace with Arnoldi"
			t = vA - np.dot(Q,np.dot(rt.conjt(Q),vA))
			continue

		'''Approximativly solve (I-QQ*)(A-rii*I)(I-QQ*)t=-res'''
		CorrFunc = ssl.LinearOperator(A.shape,lambda x:mv(A,x,Q,R))
		y = np.dot(rt.conjt(Q),res)
		res = res-Q*y
		t2 = time.time()-t1-itstart
		if adapt:
			#t = test.gmres(CorrFunc,-res,eps=la.norm(res)*gmres_tol,maxiter=1,restart=10)
			(t,info) = ssl.gmres(CorrFunc,-res,tol=la.norm(res)*gmres_tol,maxiter=1,restart=10)
		else:
			#t = test.gmres(CorrFunc,-res,eps=gmres_tol,maxiter=1,restart=10)
			(t,info) = ssl.gmres(CorrFunc,-res,tol=gmres_tol,maxiter=1,restart=10)
		t3 = time.time()-t2-t1-itstart
		gmreszeit = gmreszeit+t3
		t = t.reshape((dim,1))
		t = t - np.dot(Q,np.dot(rt.conjt(Q),t))
		t4 = time.time()-t3-t2-t1-itstart
		rest=rest+t2+t4
		if verb > 1:
			print('t2: {:5.6f}s'.format(t2))
			print('t3: {:5.6f}s'.format(t3))
			print('t4: {:5.6f}s'.format(t4))
		if verb > 2:
			print "current eigenvalue R = ", R
			print "Orthogonality of W: max(W*W-I) = ", np.amax(np.dot(rt.conjt(W),W)-np.identity(nIter))
			print "Orthogonality of Q: Q*Q = ", np.dot(rt.conjt(Q),Q)
			print "Orthogonality of t to Q: Q*t = ", np.dot(rt.conjt(Q),t)
		itend = time.time()
		times = np.append(times,itend-itstart)
		if verb > 0:
			print "----------------------------------------------------------"
	'''not converged'''
	ende = time.time()
	print('outerzeit: {:5.3f}s'.format(outerzeit))
	print('gmreszeit: {:5.3f}s'.format(gmreszeit))
	print('restzeit: {:5.3f}s'.format(rest))
	print('zeit: {:5.3f}s'.format(ende-start))
	#JadaColorPlot(range(1,maxIter+1),plotres,eigs,'Jacobi-Davidson')
	#JadaColorPlot(range(1,maxIter+1),plotres,eigs,'SpinOpB $L=14$ Jada')
	#plt.plot(range(1,maxIter+1),times[0:maxIter])
	#plt.show()
	if nIter == maxIter:
		print "Jacobi-Davidson not converged!"
		return (np.zeros((dim,1)),0)
