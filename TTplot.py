# -*- coding: utf-8 -*-
""" Small script based on matplotlib to visiulize tensor networks, in particular tensor trains """

__all__ = ['ttplot', 'ttop_plot', 'tensorplot','ttmatvek','ttdot']
__authors__ = ['Rebekka-Sarah Hennig <Rebekka-Sarah.Hennig@dlr.de>']
__data__ =  '2020-11-09'


import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
from matplotlib.markers import MarkerStyle

'''
adding a quadratic beziercurve for the given points b0,b1,b2 to ax
'''
def bezierquad(b0,b1,b2,ax):
    t=np.arange(0,1.1,0.1)
    bx=np.zeros(11)
    by=np.zeros(11)
    for i in range(0,11):
        bx[i]=(b0[0]-2*b1[0]+b2[0])*t[i]*t[i]+(-2*b0[0]+2*b1[0])*t[i]+b0[0]
        by[i]=(b0[1]-2*b1[1]+b2[1])*t[i]*t[i]+(-2*b0[1]+2*b1[1])*t[i]+b0[1]
    ax.plot(bx,by,linewidth='3',color='k')

'''
adding a quadratic beziercurve for the given points b0,b1,b2 to ax with switched achses
'''
def bezierquad2(b0,b1,b2,ax):
    t=np.arange(0,1.1,0.1)
    bx=np.zeros(11)
    by=np.zeros(11)
    for i in range(0,11):
        bx[i]=(b0[0]-2*b1[0]+b2[0])*t[i]*t[i]+(-2*b0[0]+2*b1[0])*t[i]+b0[0]
        by[i]=(b0[1]-2*b1[1]+b2[1])*t[i]*t[i]+(-2*b0[1]+2*b1[1])*t[i]+b0[1]
    ax.plot(by,bx,linewidth='3',color='k')

'''
Plots a half filled circle orientated bottom-left or bottom-right.
This represents an left- or right-orthonormal TTcore.

Input:
    ax = Subplot
    x,y = Position of circlecenter
    which = 'left' or 'right' orientation
Output:
    ax
'''
def ortho_marker(ax,x,y,which):
    ax.plot(x,y,'wo',fillstyle='full',markersize='17',markeredgewidth='2',zorder=5)
    ax.plot(x,y,'ko',fillstyle='none',markersize='17',markeredgewidth='2',zorder=10)
    if which == 'left':
        Half=mpl.patches.Wedge((x,y),0.15,theta1=-45,theta2=135,color='k',zorder=7)
    elif which == 'right':
        Half=mpl.patches.Wedge((x,y),0.15,theta1=45,theta2=-135,color='k',zorder=7)
    else:
        print('not implemented')
    ax.add_artist(Half)
    return ax

'''
Plots a sun-like marker
This may represent an optimised TTcore, an updated TTcore or something else

Input:
    ax = subplot
    x,y = Position of circlecenter
Output:
    ax
'''
def sun_marker(ax,x,y):
    line = Line2D([x+0.26,x+0.38],[y,y],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x-0.26,x-0.38],[y,y],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x,x],[y+0.26,y+0.38],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x,x],[y-0.26,y-0.38],linewidth='2',color='k')
    ax.add_line(line)
    
    line = Line2D([x+0.7*(4/21),x+(4/21)],[y+0.7*(7/21),y+(7/21)],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x-0.7*(4/21),x-(4/21)],[y-0.7*(7/21),y-(7/21)],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x+0.7*(4/21),x+(4/21)],[y-0.7*(7/21),y-(7/21)],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x-0.7*(4/21),x-(4/21)],[y+0.7*(7/21),y+(7/21)],linewidth='2',color='k')
    ax.add_line(line)
    
    line = Line2D([x+0.7*(7/21),x+(7/21)],[y+0.7*(4/21),y+(4/21)],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x-0.7*(7/21),x-(7/21)],[y-0.7*(4/21),y-(4/21)],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x+0.7*(7/21),x+(7/21)],[y-0.7*(4/21),y-(4/21)],linewidth='2',color='k')
    ax.add_line(line)
    line = Line2D([x-0.7*(7/21),x-(7/21)],[y+0.7*(4/21),y+(4/21)],linewidth='2',color='k')
    ax.add_line(line)
    
    ax.plot(x,y,color='k',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
    return ax

'''
Creates a subplot with the graphical representation of a TT-Vektor

Input:
    ax = subplot
    dim = 1D-array with the number of dimensions for every TTcore
    kind = 1D-array with the kind of every TTcore. Possible are:
            'n': nothing special
            'l': left orthogonal
            'r': right orthogonal
            'qr': qr decomposition
			'rq': rq decomposition
            's': special (makes sun marker)
    txt = boolean that enables the mode description (default: False)
	text = gives text for the description of the Bonddimensions (default: None)
	title = gives a title added on the right of the image (default: None)
Output:
    ax
'''
def ttsubplot(ax,dim,kind,txt=False,text=None,title=None):
    ndim = np.sum(dim)
    ncores = len(dim)
    if ncores!=len(kind):
        print("Dimension missmatch!")
    dimnext=0
    xnext=0
    for i in range(0,ncores):
        x = xnext
        if dim[i] == 1:
            if i!=ncores-1:
                line = Line2D([x,x+1],[0,0],linewidth='3',color='k')
                ax.add_line(line)
                if txt:
                    if text==None:
                        if kind[i]!='qr':
                            ax.annotate(r'$r_{{{}}}$'.format(i+1),(x+0.5,0.2),ha='center',va='center',size=13)
                        else:
                            ax.annotate(r'$s_{{{}}}$'.format(i+1),(x+0.3,0.2),ha='center',va='center',size=13)
                            ax.annotate(r'$r_{{{}}}$'.format(i+1),(x+0.7,0.2),ha='center',va='center',size=13)
                    else:
                        if kind[i]=='qr':
                            ax.annotate(r'${}_{{{}}}$'.format(text[i],i+1),(x+0.3,0.2),ha='center',va='center',size=13)
                            ax.annotate(r'${}_{{{}}}$'.format(text[i+1],i+1),(x+0.7,0.2),ha='center',va='center',size=13)
                        elif kind[i+1]=='rq':
                            ax.annotate(r'${}_{{{}}}$'.format(text[i+1],i+1),(x+0.7,0.2),ha='center',va='center',size=13)
                            ax.annotate(r'${}_{{{}}}$'.format(text[i],i+1),(x+0.3,0.2),ha='center',va='center',size=13)
                        else:
                            ax.annotate(r'${}_{{{}}}$'.format(text[i+1],i+1),(x+0.5,0.2),ha='center',va='center',size=13)
                            
            line=Line2D([x,x],[0,-1],linewidth='3',color='k')
            ax.add_line(line)
            if txt:
                ax.annotate(r'$n_{{{}}}$'.format(dimnext+1),(x+0.2,-0.95),ha='center',va='center',size=13)
            xnext=x+1
            dimnext=dimnext+dim[i]
        elif dim[i] >= 2:
            if ncores!=1:
                if i==ncores-1:
                    line = Line2D([x,x+(dim[i]-1)*0.5],[0,0],linewidth='3',color='k')
                    ax.add_line(line)
                elif i==0:   
                    line = Line2D([x+(dim[i]-1)*0.5,x+dim[i]],[0,0],linewidth='3',color='k')
                    ax.add_line(line)
                    if txt:
                        ax.annotate(r'$r_{{{}}}$'.format(i+1),(x+(3*dim[i]-1)/4,0.2),ha='center',va='center',size=13)
                else:
                    line = Line2D([x,x+dim[i]],[0,0],linewidth='3',color='k')
                    ax.add_line(line)
                    if txt:
                        ax.annotate(r'$r_{{{}}}$'.format(i+1),(x+(3*dim[i]-1)/4,0.2),ha='center',va='center',size=13)

            for k in range(0,dim[i]):
                line=Line2D([x+(dim[i]-1)*0.5,x+k],[0,-1],linewidth='3',color='k')
                ax.add_line(line)
                if txt:
                    ax.annotate(r'$n_{{{}}}$'.format(dimnext+1+k),(x+k+0.2,-0.95),ha='center',va='center',size=13)
            xnext = x+dim[i]
            dimnext=dimnext+dim[i]
            x=x+(dim[i]-1)*0.5

        if kind[i]=='n':
            ax.plot(x,0,color='k',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
        elif kind[i]=='l':
            ortho_marker(ax,x,0,'left')
        elif kind[i]=='r':
            ortho_marker(ax,x,0,'right')
        elif kind[i] =='qr':
            ortho_marker(ax,x,0,'left')
            ax.plot(i+0.5,0,'k.',markersize='17') 
            ax.annotate(r'$\to$',(x+0.7,-0.1),ha='center',va='center',size=13)
        elif kind[i] =='rq':
            ortho_marker(ax,x,0,'right')
            ax.plot(i-0.5,0,'k.',markersize='17')
            ax.annotate(r'$\leftarrow$',(x-0.7,-0.1),ha='center',va='center',size=13)
        elif kind[i] =='s':
            sun_marker(ax,x,0)
    if title!=None:
        ax.annotate(r'${}$'.format(title),(xnext-0.2,-0.5),ha='left',va='center',size=18)
    ax.axis('off')
    ax.set_xlim([-0.5,ndim+0.5])
    ax.set_ylim([-1.1,0.5])
    ax.set_aspect('equal',adjustable='box')
    return ax

'''
Creates a plot of the graphical representation of some TT-Vektors.
Useful for the description of an algorithm

Input:
    nsubplots = number of TTvektors
    dim = 2D-array with the number of dimensions for every TTcore
    kind = 2D-array with the kind of every TTcore. Possible are:
            'n': nothing special
            'l': left orthogonal
            'r': right orthogonal
            'qr': qr decomposition
			'rq': rq decomposition
            's': special (makes sun marker)
    fname = filename to save figure in, if 'none' just plot it
    txt = boolean that enables the mode description (default: False)
	text = gives text for the description of the Bonddimensions (default: None)
	title = gives a title added on the right of the image (default: None)
'''
def ttplot(nsubplots,dim,kind,fname='none',txt=False,text=None,title=None):
    fig = plt.figure()
    for i in range(0,nsubplots):
        ax = fig.add_subplot(nsubplots,1,i+1)
        if text==None:
            if title==None:
                ttsubplot(ax,dim[i],kind[i],txt=txt)
            else:
                ttsubplot(ax,dim[i],kind[i],txt=txt,title=title[i])
        else:
            if title==None:
                ttsubplot(ax,dim[i],kind[i],txt=txt,text=text[i])
            else:
                ttsubplot(ax,dim[i],kind[i],txt=txt,text=text[i],title=title[i])
    fig.set_size_inches([np.sum(dim[0])+1,1.6*nsubplots+0.3*(nsubplots-1)])
    if fname=='none':
        plt.plot()
    else:
        plt.savefig(fname,dpi=500)

'''
Creates a subplot with the graphical representation of a TT-Operator

Input:
    ax = subplot
    dim = 1D-array with the number of dimensions for every TTcore
    kind = 1D-array with the kind of every TTcore. Possible are:
            'n': nothing special
            'l': left orthogonal
            'r': right orthogonal
            'qr': qr decomposition
            's': special (makes sun marker)
    txt = boolean that enables the mode description (default: False)
Output:
    ax
'''
def ttop_subplot(ax,dim,kind,txt=False):
    ndim = np.sum(dim)
    ncores = len(dim)
    if ncores!=len(kind):
        print("Dimension missmatch!")
    dimnext=0
    xnext=0
    for i in range(0,ncores):
        x = xnext
        if dim[i] == 2:
            if i!=ncores-1:
                line = Line2D([x,x+1],[0,0],linewidth='3',color='k')
                ax.add_line(line)
                if txt:
                    ax.annotate(r'$r_{{{}}}$'.format(i+1),(x+0.5,0.2),ha='center',va='center',size=13)
            line=Line2D([x,x],[0,-1],linewidth='3',color='k')
            ax.add_line(line)
            line=Line2D([x,x],[0,1],linewidth='3',color='k')
            ax.add_line(line)
            if txt:
                ax.annotate(r'$m_{{{}}}$'.format(dimnext+1),(x+0.2,-0.95),ha='center',va='center',size=13)
                ax.annotate(r'$n_{{{}}}$'.format(dimnext+1),(x+0.2,+0.9),ha='center',va='center',size=13)
            xnext=x+1
            dimnext=dimnext+1
        elif dim[i] == 4:
            if i==ncores-1:
                line = Line2D([x,x+0.5],[0,0],linewidth='3',color='k')
                ax.add_line(line)
            elif i==0:   
                line = Line2D([x+0.5,x+2],[0,0],linewidth='3',color='k')
                ax.add_line(line)
                if txt:
                    ax.annotate(r'$r_{{{}}}$'.format(i+1),(x+1,0.2),ha='center',va='center',size=13)
            else:
                line = Line2D([x,x+2],[0,0],linewidth='3',color='k')
                ax.add_line(line)
                if txt:
                    ax.annotate(r'$r_{{{}}}$'.format(i+1),(x+1,0.2),ha='center',va='center',size=13)
            line=Line2D([x+0.5,x],[0,-1],linewidth='3',color='k')
            ax.add_line(line)
            line=Line2D([x+0.5,x+1],[0,-1],linewidth='3',color='k')
            ax.add_line(line)
            line=Line2D([x+0.5,x],[0,1],linewidth='3',color='k')
            ax.add_line(line)
            line=Line2D([x+0.5,x+1],[0,1],linewidth='3',color='k')
            ax.add_line(line)
            if txt:
                ax.annotate(r'$m_{{{}}}$'.format(dimnext+1),(x+0.2,-0.95),ha='center',va='center',size=13)
                ax.annotate(r'$n_{{{}}}$'.format(dimnext+1),(x+0.2,+0.9),ha='center',va='center',size=13)
                ax.annotate(r'$m_{{{}}}$'.format(dimnext+2),(x+1.2,-0.95),ha='center',va='center',size=13)
                ax.annotate(r'$n_{{{}}}$'.format(dimnext+2),(x+1.2,+0.9),ha='center',va='center',size=13)
            xnext = x+2
            dimnext=dimnext+2
            x=x+0.5
        elif dim[i] >2:
            print("not implemented")
        
        if kind[i]=='n':
            ax.plot(x,0,color='k',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
        elif kind[i]=='l':
            ortho_marker(ax,x,0,'left')
        elif kind[i]=='r':
            ortho_marker(ax,x,0,'right')
        elif kind[i] =='qr':
            ortho_marker(ax,x,0,'left')
            ax.plot(i+0.5,0,'k.',markersize='17')            
        elif kind[i] =='s':
            sun_marker(ax,x,0)
    ax.axis('off')
    ax.set_xlim([-0.5,ndim/2-0.5])
    ax.set_ylim([-1.1,1.1])
    ax.set_aspect('equal',adjustable='box')
    return ax

'''
Creates a plot of the graphical representation of some TT-Operators.
Useful for the description of an algorithm

Input:
    nsubplots = number of TToperators
    dim = 2D-array with the number of dimensions for every TTcore
    kind = 2D-array with the kind of every TTcore. Possible are:
            'n': nothing special
            'l': left orthogonal
            'r': right orthogonal
            'qr': qr decomposition
            's': special (makes sun marker)
    fname = filename to save figure in, if 'none' just plot it
    txt = boolean that enables the mode description (default: False)
'''
def ttop_plot(nsubplots,dim,kind,fname='none',txt=False):
    fig = plt.figure()
    for i in range(0,nsubplots):
        ax = fig.add_subplot(nsubplots,1,i+1)
        ttop_subplot(ax,dim[i],kind[i],txt=txt)
    fig.set_size_inches([np.sum(dim[0])/2+1,2.2*nsubplots+0.3*(nsubplots-1)])
    if fname=='none':
        plt.plot()
    else:
        plt.savefig(fname,dpi=500)

'''
Creates a subplot with the graphical representation of a tensor and tensor operations

Input:
    ax = subplot
    dim = 1D-array with the number of dimensions for every tensor
    contr = 1D-array with the number of contracted indizes between neighbouring tensors
    kind = 1D-array with the kind of every tensor. Possible are:
            'n': nothing special
            'o': orthogonal
            WARNING: 'o' is not really tested, so it might not look as aspected!
    txt = boolean that enables the mode description (default: False)
Output:
    ax
'''
def tensorsubplot(ax,dim,contr,kind,txt=False):
    ndim = np.sum(dim)
    ntens = len(dim)
    if ntens!=len(kind):
        print("Dimension missmatch!")
    
    xnext=0
    if ntens == 1:
        if dim[0]!=0:
            angle = 360/dim[0]
            for i in range(0,dim[0]):
                line = Line2D([0,-np.cos(np.deg2rad(i*angle))],[0,np.sin(np.deg2rad(i*angle))],linewidth='3',color='k')
                ax.add_line(line)
                if txt:
                    ax.annotate(r'$n_{{{}}}$'.format(i+1),(0.9*-np.cos(np.deg2rad(i*(angle)+15)),0.9*np.sin(np.deg2rad(i*(angle)+15))),ha='center',va='center',size=13)
        if kind[0]=='n':
            ax.plot(0,0,color='k',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
        elif kind[0]=='l':
            ax.plot(0,0,color='k',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='left')
    else: 
        dimnext=1
        for i in range(0,ntens):
            x = xnext
            if i==0:
                rest=dim[0]-contr[0]
                if rest!=0:
                    angle = 180/rest
                    for j in range(0,rest):
                        line = Line2D([x,x-np.cos(np.deg2rad(j*angle+angle/2+270))],[0,np.sin(np.deg2rad(j*angle+angle/2+270))],linewidth='3',color='k')
                        ax.add_line(line)
                        if txt:
                            ax.annotate(r'$n_{{{}}}$'.format(dimnext),(0.9*(-np.cos(np.deg2rad(j*(angle)+angle/2+270+15))),0.9*np.sin(np.deg2rad(j*(angle)+angle/2+270+15))),ha='center',va='center',size=13)
                            dimnext=dimnext+1
            elif i==ntens-1:
                angle=90/contr[i-1]
                for j in range(0,contr[i-1]):
                    hight = 2*np.sin(np.deg2rad(j*angle+angle/2+135))
                    bezierquad([x-1,0],[x-0.5,hight],[x,0],ax)
                    if txt:
                        ax.annotate(r'$n_{{{}}}$'.format(dimnext),(x-0.5,0.5*hight+0.2),ha='center',va='center',size=13)
                        dimnext=dimnext+1
                rest=dim[i]-contr[i-1]
                if rest!=0:
                    angle = 180/rest
                    for j in range(0,rest):
                        line = Line2D([x,x-np.cos(np.deg2rad(j*angle+angle/2+90))],[0,np.sin(np.deg2rad(j*angle+angle/2+90))],linewidth='3',color='k')
                        ax.add_line(line)
                        if txt:
                            ax.annotate(r'$n_{{{}}}$'.format(dimnext),(x-0.9*(np.cos(np.deg2rad(j*(angle)+angle/2+90+15))),0.9*np.sin(np.deg2rad(j*(angle)+angle/2+90+15))),ha='center',va='center',size=13)
                            dimnext=dimnext+1
                
            else:
                angle=90/contr[i-1]
                for j in range(0,contr[i-1]):
                    hight = 2*np.sin(np.deg2rad(j*angle+angle/2+135))
                    bezierquad([x-1,0],[x-0.5,hight],[x,0],ax)
                    if txt:
                        ax.annotate(r'$n_{{{}}}$'.format(dimnext),(x-0.5,0.5*hight+0.2),ha='center',va='center',size=13)
                        dimnext=dimnext+1
                rest = dim[i]-contr[i]-contr[i-1]
                rest1=int(rest/2)
                
                rest2=rest-rest1
                if rest1!=0:
                    angle1 = 90/rest1
                    for j in range(0,rest1):
                        line = Line2D([x,x-np.cos(np.deg2rad(j*angle1+angle1/2+45))],[0,np.sin(np.deg2rad(j*angle1+angle1/2+45))],linewidth='3',color='k')
                        ax.add_line(line)
                        if txt:
                            ax.annotate(r'$n_{{{}}}$'.format(dimnext),(x-0.9*(np.cos(np.deg2rad(j*(angle1)+angle1/2+45+15))),0.9*np.sin(np.deg2rad(j*(angle1)+angle1/2+45+15))),ha='center',va='center',size=13)
                            dimnext=dimnext+1
                if rest2!=0:
                    angle2 =90/rest2
                    for j in range(0,rest2):
                        line = Line2D([x,x-np.cos(np.deg2rad(j*angle2+angle2/2+225))],[0,np.sin(np.deg2rad(j*angle2+angle2/2+225))],linewidth='3',color='k')
                        ax.add_line(line)
                        if txt:
                            ax.annotate(r'$n_{{{}}}$'.format(dimnext),(x-0.9*(np.cos(np.deg2rad(j*(angle2)+angle2/2+225+15))),0.9*np.sin(np.deg2rad(j*(angle2)+angle2/2+225+15))),ha='center',va='center',size=13)
                            dimnext=dimnext+1   
            xnext=x+1        
            if kind[i]=='n':
                ax.plot(x,0,color='k',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
            elif kind[i]=='l':
                ax.plot(0,0,color='k',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='left')

    ax.axis('off')
    #ax.set_xlim([-0.5,ndim-0.5])
    #ax.set_ylim([-1.1,0.5])
    ax.set_xlim([-1.1,ntens+0.1])
    ax.set_ylim([-1.1,1.1])
    ax.set_aspect('equal',adjustable='box')
    return ax

'''
Creates a plot of the graphical representation of some tensors and tensor operations.
Useful for the description of an algorithm

Input:
    nsubplots = number of TTvektors
    dim = 1D-array with the number of dimensions for every tensor
    contr = 1D-array with the number of contracted indizes between neighbouring tensors
    kind = 1D-array with the kind of every tensor. Possible are:
            'n': nothing special
            'o': orthogonal
            WARNING: 'o' is not really tested, so it might not look as aspected!
    fname = filename to save figure in, if 'none' just plot it
    txt = boolean that enables the mode description (default: False)
'''
def tensorplot(nsubplots,dim,contr,kind,fname='none',txt=False):
    fig = plt.figure()
    for i in range(0,nsubplots):
        ax = fig.add_subplot(nsubplots,1,i+1)
        tensorsubplot(ax,dim[i],contr[i],kind[i],txt=txt)
    fig.set_size_inches([len(dim[0])+1.2,2.2*nsubplots+0.3*(nsubplots-1)])
    if fname=='none':
        plt.plot()
    else:
        plt.savefig(fname,dpi=500)

'''
visualization of matrix-vektor-produkt in TT-format
'''
def ttmatvek():
    fig=plt.figure()
    ax = plt.gca()
    y=[[1,1,1,1],[1.5,1.5,1.5,1.5],[2,2,2,2],[4,4,4,4],[4,4,4,4]]
    x=[[1,2,3,4],[5.5,6.5,7.5,8.5],[1,2,3,4],[1,2,3,4],[5.5,6.5,7.5,8.5]]
    
    for j in range(4,-1,-1):
        for i in range(0,4):
            line = Line2D([x[j][i],x[j][i]],[y[j][i],y[j][i]-1],linewidth='3',color='k')
            ax.add_line(line)
            if j==3:
                line = Line2D([x[j][i],x[j][i]],[y[j][i],y[j][i]+1],linewidth='3',color='k')
                ax.add_line(line)
                ax.annotate(r'$p_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]-0.95),ha='center',va='center',size=13)
                ax.annotate(r'$n_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]+0.9),ha='center',va='center',size=13)
                if i!=3:
                    line = Line2D([x[j][i],x[j][i]+1],[y[j][i],y[j][i]],linewidth='3',color='k')
                    ax.add_line(line)
                    ax.annotate(r'$s_{{{}}}$'.format(i+1),(x[j][i]+0.5,y[j][i]+0.2),ha='center',va='center',size=13)
                ax.plot(x[j][i],y[j][i],color='b',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
            if j==4:
                ax.annotate(r'$n_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]-0.95),ha='center',va='center',size=13)
                if i!=3:
                    line = Line2D([x[j][i],x[j][i]+1],[y[j][i],y[j][i]],linewidth='3',color='k')
                    ax.add_line(line)
                    ax.annotate(r'$r_{{{}}}$'.format(i+1),(x[j][i]+0.5,y[j][i]+0.2),ha='center',va='center',size=13)
                ax.plot(x[j][i],y[j][i],color='r',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
            if j==2:
                ax.annotate(r'$n_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]-0.5),ha='center',va='center',size=13)
                if i!=3:
                    line = Line2D([x[j][i],x[j][i]+1],[y[j][i],y[j][i]],linewidth='3',color='k')
                    ax.add_line(line)
                    ax.annotate(r'$r_{{{}}}$'.format(i+1),(x[j][i]+0.5,y[j][i]+0.2),ha='center',va='center',size=13)
                ax.plot(x[j][i],y[j][i],color='r',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
            if j==1:
                ax.annotate(r'$p_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]-0.95),ha='center',va='center',size=13)
                if i!=3:
                    line = Line2D([x[j][i],x[j][i]+1],[y[j][i],y[j][i]],linewidth='3',color='k')
                    ax.add_line(line)
                    ax.annotate(r'$r_{{{}}}\cdot s_{{{}}}$'.format(i+1,i+1),(x[j][i]+0.5,y[j][i]+0.2),ha='center',va='center',size=13)
                ax.plot(x[j][i],y[j][i],color='m',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
            if j==0:
                ax.annotate(r'$p_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]-0.95),ha='center',va='center',size=13)
                if i!=3:
                    line = Line2D([x[j][i],x[j][i]+1],[y[j][i],y[j][i]],linewidth='3',color='k')
                    ax.add_line(line)
                    ax.annotate(r'$s_{{{}}}$'.format(i+1),(x[j][i]+0.5,y[j][i]+0.2),ha='center',va='center',size=13)
                ax.plot(x[j][i],y[j][i],color='b',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
                line = Line2D([x[j][i]-0.25,x[j][i]-0.25],[y[j][i]-0.25,y[2][i]+0.25],linestyle=':',linewidth=2,color='k')
                ax.add_line(line)
                line = Line2D([x[j][i]+0.25,x[j][i]+0.25],[y[j][i]-0.25,y[2][i]-0.6],linestyle=':',linewidth=2,color='k')
                ax.add_line(line)
                line = Line2D([x[j][i]+0.25,x[j][i]+0.25],[y[2][i]-0.4,y[2][i]+0.25],linestyle=':',linewidth=2,color='k')
                ax.add_line(line)
                line = Line2D([x[j][i]-0.25,x[j][i]+0.25],[y[2][i]+0.25,y[2][i]+0.25],linestyle=':',linewidth=2,color='k')
                ax.add_line(line)
                line = Line2D([x[j][i]-0.25,x[j][i]+0.25],[y[j][i]-0.25,y[j][i]-0.25],linestyle=':',linewidth=2,color='k')
                ax.add_line(line)
                
    ax.annotate(r'$\langle$',(x[3][0]-0.75,y[3][0]),ha='center',va='center',size=30)
    ax.annotate(r'$,$',(x[3][3]+0.75,y[3][0]),ha='center',va='center',size=30)
    ax.annotate(r'$\rangle_{N}$',(x[4][3]+0.75,y[4][3]),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[0][0]-0.75,y[1][0]),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[0][3]+0.75,y[1][0]),ha='center',va='center',size=30)
    ax.axis('off')
    ax.set_xlim([0,9.5])
    ax.set_ylim([0,5])
    ax.set_aspect('equal',adjustable='box')
    fig.set_size_inches([9.5,5])
    plt.savefig('ttmatvek',dpi=500)
    plt.plot()

'''
visualization of skalarprodukt in TT-format
'''
def ttdot():
    fig=plt.figure()
    ax = plt.gca()
    x=[[1,2,3,4],[5.5,6.5,7.5,8.5],[1,2,3,4],[5.5,6.5,7.5],[1,2,3],[5.5,6.5],[1,2],[4],[6.5]]
    y=[[8,8,8,8],[8,8,8,8],[6,6,6,6],[6,6,6],[4,4,4],[4,4],[2,2],[2],[2]]
    
    for j in range(0,9):
        k=len(x[j])
        if (j==3) or (j==5) or (j==7):
            line = Line2D([x[j][k-1],x[j][k-1]+1],[y[j][k-1],y[j][k-1]-0.5],linewidth='3',color='k')
            ax.add_line(line)
            line = Line2D([x[j][k-1],x[j][k-1]+1],[y[j][k-1]-1,y[j][k-1]-0.5],linewidth='3',color='k')
            ax.add_line(line)
            ax.annotate(r'$r_{{{}}}$'.format(k),(x[j][k-1]+0.65,y[j][k-1]-0.1),ha='center',va='center',size=13)
            ax.annotate(r'$s_{{{}}}$'.format(k),(x[j][k-1]+0.65,y[j][k-1]-0.85),ha='center',va='center',size=13)
            ax.plot(x[j][k-1]+1,y[j][k-1]-0.5,color='m',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
            line = Line2D([x[j][k-1]-0.3,x[j][k-1]+1.1],[y[j][k-1]-0.9,y[j][k-1]-0.2],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
            line = Line2D([x[j][k-1]-0.1,x[j][k-1]+1.3],[y[j][k-1]-1.4,y[j][k-1]-0.7],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
            line = Line2D([x[j][k-1]-0.3,x[j][k-1]-0.05],[y[j][k-1]-0.9,y[j][k-1]-1.4],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
            line = Line2D([x[j][k-1]+1.1,x[j][k-1]+1.35],[y[j][k-1]-0.2,y[j][k-1]-0.7],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
        if (j==4) or (j==6) or (j==8):    
            bezierquad2([y[j][k-1],x[j][k-1]],[y[j][k-1]-0.5,x[j][k-1]+1],[y[j][k-1]-1,x[j][k-1]],ax)
            ax.annotate(r'$r_{{{}}}$'.format(k),(x[j][k-1]+0.7,y[j][k-1]-0.5),ha='center',va='center',size=13)
            line = Line2D([x[j][k-1]-0.25,x[j][k-1]-0.25],[y[j][k-1]+0.25,y[j][k-1]-1.25],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
            line = Line2D([x[j][k-1]+0.9,x[j][k-1]+0.9],[y[j][k-1]+0.25,y[j][k-1]-1.25],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
            line = Line2D([x[j][k-1]-0.25,x[j][k-1]+0.9],[y[j][k-1]+0.25,y[j][k-1]+0.25],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
            line = Line2D([x[j][k-1]-0.25,x[j][k-1]+0.9],[y[j][k-1]-1.25,y[j][k-1]-1.25],linestyle=':',linewidth=2,color='k')
            ax.add_line(line)
        for i in range(0,k):
            line = Line2D([x[j][i],x[j][i]],[y[j][i],y[j][i]-1],linewidth='3',color='k')
            ax.add_line(line)
            if i!=k-1:
                line = Line2D([x[j][i],x[j][i]+1],[y[j][i],y[j][i]],linewidth='3',color='k')
                ax.add_line(line)
                if j!=1:
                    ax.annotate(r'$r_{{{}}}$'.format(i+1,i+1),(x[j][i]+0.5,y[j][i]+0.2),ha='center',va='center',size=13)
                else:
                    ax.annotate(r'$s_{{{}}}$'.format(i+1,i+1),(x[j][i]+0.5,y[j][i]+0.2),ha='center',va='center',size=13)
                if j>1:
                    
                    line = Line2D([x[j][i],x[j][i]+1],[y[j][i]-1,y[j][i]-1],linewidth='3',color='k')
                    ax.add_line(line)
                    ax.annotate(r'$s_{{{}}}$'.format(i+1,i+1),(x[j][i]+0.5,y[j][i]-0.8),ha='center',va='center',size=13)
            if j>1:
                ax.annotate(r'$n_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]-0.5),ha='center',va='center',size=13)
                ax.plot(x[j][i],y[j][i],color='r',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
                if (i==k-1) and ((j==4) or (j==6) or (j==8)):
                    ax.plot(x[j][i],y[j][i]-1,color='b',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
                else:
                    ax.plot(x[j][i],y[j][i]-1,color='g',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
            else:
                ax.annotate(r'$n_{{{}}}$'.format(i+1),(x[j][i]+0.2,y[j][i]-0.95),ha='center',va='center',size=13)
                if j==0:
                    ax.plot(x[j][i],y[j][i],color='r',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
                else:
                    ax.plot(x[j][i],y[j][i],color='g',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
    
    ax.plot(8.5,1.5,color='m',marker='o',markerfacecoloralt='w',markersize='17',markeredgewidth='2',fillstyle='full')
    ax.annotate(r'$\mathrm{dot}($',(x[0][0]-0.75,y[0][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$,$',(x[1][0]-0.75,y[1][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$)$',(x[1][3]+0.25,y[1][3]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[2][0]-0.75,y[2][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[3][0]-0.75,y[3][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[4][0]-0.75,y[4][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[5][0]-0.75,y[5][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[6][0]-0.75,y[6][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[7][0]-0.75,y[7][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[8][0]-0.75,y[8][0]-0.5),ha='center',va='center',size=30)
    ax.annotate(r'$=$',(x[8][0]+1.25,y[8][0]-0.5),ha='center',va='center',size=30)
    
    k=len(x[2])
    line = Line2D([x[2][k-1]-0.25,x[2][k-1]-0.25],[y[2][k-1]+0.25,y[2][k-1]-1.25],linestyle=':',linewidth=2,color='k')
    ax.add_line(line)
    line = Line2D([x[2][k-1]+0.4,x[2][k-1]+0.4],[y[2][k-1]+0.25,y[2][k-1]-1.25],linestyle=':',linewidth=2,color='k')
    ax.add_line(line)
    line = Line2D([x[2][k-1]-0.25,x[2][k-1]+0.4],[y[2][k-1]+0.25,y[2][k-1]+0.25],linestyle=':',linewidth=2,color='k')
    ax.add_line(line)
    line = Line2D([x[2][k-1]-0.25,x[2][k-1]+0.4],[y[2][k-1]-1.25,y[2][k-1]-1.25],linestyle=':',linewidth=2,color='k')
    ax.add_line(line)
    
    ax.axis('off')
    ax.set_xlim([0,9.5])
    ax.set_ylim([0,8.5])
    ax.set_aspect('equal',adjustable='box')
    fig.set_size_inches([9.5,8.5])    
    plt.savefig('ttdot',dpi=500)
    plt.plot()

if __name__ == '__main__':
    '''
    markertest
    '''
    testdim = [[1,1,1,1]]
    testkind = [['n','n','n','n']]
    ttplot(1,testdim,testkind,txt=True)
    
    '''
    Operatortest
    '''
    testdim = [[2,2,2,2]]
    testkind = [['n','n','n','n']]
    ttop_plot(1,testdim,testkind,txt=True)
    
    '''
    example ALS algo half sweep
    '''
    nsubplots=6
    dim = [[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]]
    kind = [['s','r','r','r'],['qr','r','r','r'],['l','s','r','r'],['l','qr','r','r'],['l','l','s','r'],['l','l','qr','r']]
    title=['\mathrm{Opt}_1','\mathrm{QR}_1','\mathrm{Opt}_2','\mathrm{QR}_2','\mathrm{Opt}_3','\mathrm{QR}_3']
    ttplot(nsubplots,dim,kind,txt=False,fname='ALS',title=title)

    '''
    example MALS algo half sweep
    '''
    nsubplots=4
    dim = [[2,1,1],[1,1,1,1],[1,2,1],[1,1,1,1]]
    kind = [['s','r','r'],['qr','r','r','r'],['l','s','r'],['l','qr','r','r']]
    ttplot(nsubplots,dim,kind)
    
    '''
    Tensortest
    '''
    dim=[[3]]
    kind=[['n']]
    contr=[[]]
    tensorplot(1,dim,contr,kind,txt=True,fname='3dtensor')
    dim=[[2]]
    kind=[['n']]
    contr=[[]]
    tensorplot(1,dim,contr,kind,txt=True,fname='2dtensor')
    dim=[[1]]
    kind=[['n']]
    contr=[[]]
    tensorplot(1,dim,contr,kind,txt=True,fname='1dtensor')
    dim=[[4,2]]
    kind=[['n','n']]
    contr=[[2]]
    tensorplot(1,dim,contr,kind,txt=True,fname='tensoroperation')
    dim=[[1,1]]
    kind=[['n','n']]
    contr=[[1]]
    tensorplot(1,dim,contr,kind,txt=True,fname='dot')
    dim=[[2,1]]
    kind=[['n','n']]
    contr=[[1]]
    tensorplot(1,dim,contr,kind,txt=True,fname='matvec')
    dim=[[5,6,4]]
    kind=[['n','n','n']]
    contr=[[2,3]]
    tensorplot(1,dim,contr,kind,txt=True)
    
    '''
    ttdarstellung
    '''
    testdim = [[1,1,1,1,1]]
    testkind = [['n','n','n','n','n']]
    ttplot(1,testdim,testkind,txt=True,fname='tttensor')
    testdim = [[2,2,2,2,2]]
    testkind = [['n','n','n','n','n']]
    ttop_plot(1,testdim,testkind,txt=True,fname='TTop')
    testdim = [[1,1,1,1,1]]
    testkind = [['l','l','l','l','n']]
    ttplot(1,testdim,testkind,txt=True,fname='leftortho')
    testdim = [[1,1,1,1,1]]
    testkind = [['n','r','r','r','r']]
    ttplot(1,testdim,testkind,txt=True,fname='rightortho')

    nsubplots=4
    dim = [[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]]
    kind = [['s','n','n','n'],['n','s','n','n'],['n','n','s','n'],['n','n','n','s']]
    ttplot(nsubplots,dim,kind,txt=True)
    
    dim=[[5]]
    kind=[['l']]
    contr=[[]]
    tensorplot(1,dim,contr,kind,txt=True,fname='ortho')
    
    '''
    example Truncation
    '''
    nsubplots=5
    dim = [[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]]
    kind = [['n','n','n','n'],['n','n','n','rq'],['n','n','rq','r'],['n','rq','r','r'],['n','r','r','r']]
    text=[['r','r','r','r','r'],['r','r','r','\gamma','\gamma'],['r','r','\gamma','\gamma','\gamma'],['r','\gamma','\gamma','\gamma','\gamma'],['\gamma','\gamma','\gamma','\gamma','\gamma']]
    title=['T','\mathrm{RQ}_4','\mathrm{RQ}_3','\mathrm{RQ}_2','\~T']
    ttplot(nsubplots,dim,kind,txt=True,text=text,fname='Truncation1',title=title)
    nsubplots=5
    dim = [[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]]
    kind = [['n','r','r','r'],['qr','r','r','r'],['l','qr','r','r'],['l','l','qr','r'],['l','l','l','n']]
    text=[['\gamma','\gamma','\gamma','\gamma','\gamma'],['s','\gamma','\gamma','\gamma','\gamma'],['s','s','\gamma','\gamma','\gamma'],['s','s','s','\gamma','\gamma'],['s','s','s','s','s']]
    title=['\~T','\mathrm{SVD}_1','\mathrm{SVD}_2','\mathrm{SVD}_3','B']
    ttplot(nsubplots,dim,kind,txt=True,text=text,fname='Truncation2',title=title)
    
    '''
    Truncation2
    '''
    nsubplots=5
    dim = [[4],[1,3],[1,1,2],[1,1,1,1],[1,1,1,1]]
    kind = [['n'],['qr','n'],['l','qr','n'],['l','l','qr','n'],['l','l','l','n']]
    title=['T','\mathrm{SVD}_1','\mathrm{SVD}_2','\mathrm{SVD}_3','B']
    text=[['r'],['r','r'],['r','r','r'],['r','r','r','r'],['r','r','r','r','r']]
    ttplot(nsubplots,dim,kind,txt=True,text=text,fname='Truncation3',title=title)
    
	'''
	special functions
	'''
    ttmatvek()
    
    ttdot()
