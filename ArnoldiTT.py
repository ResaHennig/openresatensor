import numpy as np
import numpy.linalg as la
import scipy.linalg as sla
import resaTT.tools as rtt
import resatools as rt
import scipy.io as sio
import tt as tt
import time

def Arnoldi(ATT,tTT,epsilon,maxIter,restarts,verb=0):
	start=time.time()
	t_n = tTT.n
	dim = np.prod(t_n)
	
	v = tTT*(1/tTT.norm())	
	for k in range(0,restarts):
		if verb>0:
			print "restart",k
		H = np.zeros((1,0))
		W = []
		nIter = maxIter
		for j in range(1,maxIter+1):
			W = np.append(W,v)
			vA = rtt.matvec(ATT,v)
			vA = rtt.round(vA,epsilon*1e-2)
			H = np.append(H,np.zeros((j,1)),axis=1)
			for i in range(1,j+1):
				H[i-1,j-1] = tt.dot(rtt.conjt(vA),W[i-1])
			v = vA
			for i in range(1,j+1):
				v = v - H[i-1,j-1]*W[i-1]
				v = rtt.round(v,epsilon*1e-2)
			H = np.append(H,np.zeros((1,j)),axis=0)
			H[j,j-1] = v.norm()
		
			if H[j,j-1]<1e-14:
				nIter = j
				break
			v = v*(1/H[j,j-1])
	
		(RH,QH) = la.eig(H[0:nIter,0:nIter])
		ind = np.argmin(RH)
		R = RH[ind]
		Q = rtt.zeros(t_n)
		for i in range(0,nIter):
			Q = Q + W[i]*QH[i,ind]
			Q=rtt.round(Q,epsilon*1e-2)
		res = rtt.matvec(ATT,Q)-Q*R
		res=rtt.round(res,epsilon*1e-2)
		normres = res.norm()
		if verb>0:
			print("normres "+str(normres))
			print("rmax v "+str(rtt.get_rmax(v)))
			print("rmax q "+str(rtt.get_rmax(Q)))
		if normres<epsilon:
			ende = time.time()
			print("Arnoldi converged")
			print('zeit: {:5.3f}s'.format(ende-start))
			return (Q,R)
		v = Q*(1/Q.norm())
	ende = time.time()
	print("Arnoldi not converged")
	print('zeit: {:5.3f}s'.format(ende-start))
	return (Q,R)
	
def ArnoldifromJada(ATT, tTT, epsilon, maxIter,verb=0):
	print('ArnoldiTTfromJada')
	'''initialisation'''
	start=time.time()
	t_n = tTT.n
	dim = np.prod(t_n)
	H = np.zeros((0,0))
	W = []
	WA = []

	'''outer iteration'''
	for nIter in range(1,maxIter+1):
		'''orthogonalize new searchdirection to searchspace'''
		v = tTT
		for i in range(0,nIter-1):
			y = tt.dot(W[i],tTT)
			v = v - y*W[i]
			v = rtt.round(v,epsilon*1e-2)
		v = v * (1/v.norm())
		for i in range(0,nIter-1):
			y = tt.dot(W[i],v)
			v = v - y*W[i]
			v = rtt.round(v,epsilon*1e-2)
		v = v * (1/v.norm())
		
		'''update projection of A onto searchspace'''
		vA = rtt.matvec(ATT,v)
		
		H = np.append(H,np.zeros((nIter-1,1)),axis=1)
		for i in range(0,nIter-1):
			H[i,nIter-1] = tt.dot(rtt.conjt(vA),W[i])
		H = np.append(H,np.zeros((1,nIter)),axis=0)
		for i in range(0,nIter-1):
			H[nIter-1,i] = tt.dot(rtt.conjt(v),WA[i])
		H[nIter-1,nIter-1]= tt.dot(rtt.conjt(v),vA)
		W = np.append(W,v)
		WA = np.append(WA,vA)
		
		'''Compute Schur decomposition H QH = QH RH'''
		(RH,QH) = la.eig(H) #eigenvalues not sorted
		ind = np.argmin(RH)
		R = RH[ind]
		Q = rtt.zeros(t_n)
		QA = rtt.zeros(t_n)
		for i in range(0,nIter):
			Q = Q + W[i]*QH[i,ind]
			QA = QA + WA[i]*QH[i,ind]
			Q=rtt.round(Q,epsilon*1e-2)
			QA = rtt.round(QA,epsilon*1e-2)

		'''Compute Residual and check tolerance'''
		res = (QA-Q*R)
		res = rtt.round(res,epsilon*1e-2)
		normres = res.norm()
		
		if verb > 0:
			print "Iteration",nIter,":"
			print "res", normres
			print "eig",R
			print "bond qtt",rtt.get_rmax(Q)
			print "bond res",rtt.get_rmax(res)
		if normres < epsilon:
			ende = time.time()
			print('zeit: {:5.3f}s'.format(ende-start))
			return (Q,R)
		
		tTT = vA - Q*tt.dot(rtt.conjt(Q),vA)
		tTT = rtt.round(tTT,epsilon*1e-2)
		
	'''not converged'''
	ende = time.time()
	print('zeit: {:5.3f}s'.format(ende-start))
	if nIter == maxIter:
		print "Arnoldi not converged!"
		return (Q,R)
	
