'''
Spinchain-Benchmark

L Elektrons in a chain. Each one can have spin-up (1) or spin-down (0).
=> 2^L possible states which can be created with spinvec_create-routines.

two neighbouring elektrons can switch their spins or stay the same
=> TTspinOp(L) = Hamilton-Operator for Transition between the possible chainstates in TT-Format
   (spinOp(L) = Hamilton-Operator in canonical-Format)

TODO: there could be an external B-Feld which interacts with the spins
'''

import tt as tt
import numpy as np
import numpy.linalg as la

'''
creation of spinvector

Input:
L: number of spins
binary: array with states of spins (0 or 1)

Output:
c: spinvector in TT-Format
'''
def spinvec_create(L,binary):
	c = tt.vector()
	c.d = L
	c.n = np.array([2]*L,dtype=np.int32)
	c.r = np.array([1]*(L+1),dtype=np.int32)
	c.get_ps()					# ps[i] tells you, where the i-th core starts in c.core
	c.core = np.zeros(c.ps[c.d]-1)
	for i in range(0,L):
		if binary[i]==0: 			#spin-down
			c.core[c.ps[i]]=1
		else: 					# spin-up
			c.core[c.ps[i]-1]=1
	return c

'''
creates spinvector from the decimal-number of the spinchainstate
'''
def spinvec_from_decimal(L, which):
	binary = bin(which)[2:] 			#decimal to binary
	if len(binary)>L:
		print("this state does not exist when looking at {} spins.".format(L))
		exit()
	binary = [int(x) for x in '0'*(L-len(binary))+binary] #add leading zeros
	return spinvec_create(L,binary)

'''
creates spinvector from the binary number of the spinchain-state
'''
def spinvec_from_binary(L,which):
	binary = [int(x) for x in which]
	if len(binary)>L:
		print("this state does not exist when looking at {} spins.".format(L))
		exit()
	return spinvec_create(L,binary)

'''
Spinoperator for staying the same
'''
def TTspinOp_Sz():
	Sz_op = tt.eye(2,2)
	Sz_op.tt.core[Sz_op.tt.ps[0]+2]=-1
	Sz_op.tt.core[Sz_op.tt.ps[1]+2]=-1
	return Sz_op

'''
TTspinOp-helper:
Spinoperator for flipping i-th up, (i+1)-th down
'''
def TTspinOp_Sp():
	Sp_op = tt.eye(2,2)
	Sp_op.tt.core[Sp_op.tt.ps[0]-1]=0
	Sp_op.tt.core[Sp_op.tt.ps[0]+1]=2
	Sp_op.tt.core[Sp_op.tt.ps[0]+2]=0
	Sp_op.tt.core[Sp_op.tt.ps[1]-1]=0
	Sp_op.tt.core[Sp_op.tt.ps[1]]=1
	Sp_op.tt.core[Sp_op.tt.ps[1]+2]=0
	return Sp_op

'''
TTspinOp-helper:
Spinoperator for flipping i-th down, (i+1)-th up
'''
def TTspinOp_Sn():
	Sn_op = tt.eye(2,2)
	Sn_op.tt.core[Sn_op.tt.ps[0]-1]=0
	Sn_op.tt.core[Sn_op.tt.ps[0]]=1
	Sn_op.tt.core[Sn_op.tt.ps[0]+2]=0
	Sn_op.tt.core[Sn_op.tt.ps[1]-1]=0
	Sn_op.tt.core[Sn_op.tt.ps[1]+1]=2
	Sn_op.tt.core[Sn_op.tt.ps[1]+2]=0
	return Sn_op

'''
TTspinOp-helper:
spinoperator for interaction between two spins
'''
def TTspinOp_nachbar():
	spinop_n = TTspinOp_Sz().__add__(TTspinOp_Sp())
	spinop_n = spinop_n.__add__(TTspinOp_Sn())
	return spinop_n

'''
TTspinOp-helper:
spinoperator if only the i-th and (i+1)-th spin could interact
'''
def TTspinOp_I(L,i):
	if i!=L-1:
		Ia = tt.eye(2,i)
		Ib = tt.eye(2,L-(i+2))
		spinop_I = Ia.__kron__(TTspinOp_nachbar().__kron__(Ib))
	else:
		spinop_I = spinOp_I(L,i)
	return spinop_I

'''
Creation of spinOperator in TT-Format

Input:
L: number of spins

Output:
spinop: spinOperator in TT-Format with periodic boundaryconditions
'''
def TTspinOp(L):
	spinop=TTspinOp_I(L,0)
	for i in range(1,L):
		spinop = spinop.__add__(TTspinOp_I(L,i))
	return spinop

'''
Creation of non-periodic spinOperator in TT-Format

Input:
L: number of spins

Output:
spinop: spinOperator in TT-Format without periodic boundarycondition
'''
def nonperiodTTspinOp(L):
	spinop=TTspinOp_I(L,0)
	for i in range(1,L-1):
		spinop = spinop.__add__(TTspinOp_I(L,i))
	return spinop

'''
spinOp-helper:
Spinoperator for staying the same
'''
def spinOp_Sz(L,i):
	Sz_op = tt.eye(2,L)
	Sz_op.tt.core[Sz_op.tt.ps[i]+2]=-1
	if i!=L-1:
		Sz_op.tt.core[Sz_op.tt.ps[i+1]+2]=-1
	else:
		Sz_op.tt.core[Sz_op.tt.ps[0]+2]=-1
	return Sz_op

'''
spinOp-helper:
Spinoperator for flipping i-th up, (i+1)-th down
'''
def spinOp_Sp(L,i):
	Sp_op = tt.eye(2,L)
	Sp_op.tt.core[Sp_op.tt.ps[i]-1]=0
	Sp_op.tt.core[Sp_op.tt.ps[i]+1]=2
	Sp_op.tt.core[Sp_op.tt.ps[i]+2]=0
	if i!=L-1:
		Sp_op.tt.core[Sp_op.tt.ps[i+1]-1]=0
		Sp_op.tt.core[Sp_op.tt.ps[i+1]]=1
		Sp_op.tt.core[Sp_op.tt.ps[i+1]+2]=0
	else:
		Sp_op.tt.core[Sp_op.tt.ps[0]-1]=0
		Sp_op.tt.core[Sp_op.tt.ps[0]]=1
		Sp_op.tt.core[Sp_op.tt.ps[0]+2]=0
	return Sp_op

'''
spinOp-helper:
Spinoperator for flipping i-th down, (i+1)-th up
'''
def spinOp_Sn(L,i):
	Sn_op = tt.eye(2,L)
	Sn_op.tt.core[Sn_op.tt.ps[i]-1]=0
	Sn_op.tt.core[Sn_op.tt.ps[i]]=1
	Sn_op.tt.core[Sn_op.tt.ps[i]+2]=0
	if i!=L-1:
		Sn_op.tt.core[Sn_op.tt.ps[i+1]-1]=0
		Sn_op.tt.core[Sn_op.tt.ps[i+1]+1]=2
		Sn_op.tt.core[Sn_op.tt.ps[i+1]+2]=0
	else:
		Sn_op.tt.core[Sn_op.tt.ps[0]-1]=0
		Sn_op.tt.core[Sn_op.tt.ps[0]+1]=2
		Sn_op.tt.core[Sn_op.tt.ps[0]+2]=0
	return Sn_op

'''
spinOp-helper:
spinoperator if only the i-th and (i+1)-th spin could interact
'''
def spinOp_I(L,i):
	spinop_I = spinOp_Sz(L,i).__add__(spinOp_Sp(L,i))
	spinop_I = spinop_I.__add__(spinOp_Sn(L,i))
	return spinop_I

'''
Creation of spinOperator in canonical-Format

Input:
L: number of spins

Output:
spinop: spinOperator in canonical-Format with periodic boundaryconditions
'''
def spinOp(L):
	spinop=spinOp_I(L,0)
	for i in range(1,L):
		spinop = spinop.__add__(spinOp_I(L,i))
	return spinop

def TTspinOpB_Bx():
	Bx_op = tt.eye(2,1)
	Bx_op.tt.core[Bx_op.tt.ps[0]-1]=0
	Bx_op.tt.core[Bx_op.tt.ps[0]]=1
	Bx_op.tt.core[Bx_op.tt.ps[0]+1]=1
	Bx_op.tt.core[Bx_op.tt.ps[0]+2]=0
	return Bx_op

def TTspinOpB_Bz():
	Bz_op = tt.eye(2,1)
	Bz_op.tt.core[Bz_op.tt.ps[0]-1]=-1
	return Bz_op
	
def TTspinOpB_nachbar():
	spinopB_n = TTspinOpB_Bx().__add__(TTspinOpB_Bz())
	spinopB_n = spinopB_n.__kron__(tt.eye(2,1))
	spinopB_n = spinopB_n.__add__(TTspinOp_nachbar())
	return spinopB_n

def TTspinOpB_I(L,i):
	if i!=L-1:
		Ia = tt.eye(2,i)
		Ib = tt.eye(2,L-(i+2))
		spinopB_I = Ia.__kron__(TTspinOpB_nachbar().__kron__(Ib))
	else:
		spinopB_I = TTspinOpB_Bx().__add__(TTspinOpB_Bz())
		spinopB_I = tt.eye(2,L-1).__kron__(spinopB_I)
		spinopB_I = spinopB_I.__add__(spinOp_I(L,i))
	return spinopB_I

'''
Creation of spinOperator with external magnetic field in TT-Format

Input:
L: number of spins

Output:
spinopB: spinOperator with external magnetic field in TT-Format
'''
def TTspinOpB(L):
	spinopB=TTspinOpB_I(L,0)
	for i in range(1,L):
		spinopB = spinopB.__add__(TTspinOpB_I(L,i))
	return spinopB

'''
Creation of non-periodic spinOperator with external magnetic field in TT-Format

Input:
L: number of spins

Output:
spinopB: spinOperator with external magnetic field in TT-Format without periodic boundarycondition
'''
def nonperiodTTspinOpB(L):
	spinopB=TTspinOpB_I(L,0)
	for i in range(1,L-1):
		spinopB = spinopB.__add__(TTspinOpB_I(L,i))
	spinopB_n = TTspinOpB_Bx().__add__(TTspinOpB_Bz())
	spinopB_n = tt.eye(2,L-1).__kron__(spinopB_n)
	spinopB = spinopB.__add__(spinopB_n)	
	return spinopB
	
