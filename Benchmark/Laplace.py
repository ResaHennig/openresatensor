import tt as tt
import numpy as np
import numpy.linalg as la
import scipy.sparse

def Laplace(n,d):
	data = np.array([-1*np.ones(n),2*np.ones(n),-1*np.ones(n)]) #*(n+1)*(n+1)
	diags = np.array([-1,0,1])
	A = scipy.sparse.spdiags(data,diags,n,n).toarray()
	Tridi = tt.matrix(a=A)
	Laplace = Tridi.__kron__(tt.eye(n,d-1))
	for i in range(1,d):
		Ia = tt.eye(n,i)
		Ib = tt.eye(n,d-(i+1))
		Laplacei = Ia.__kron__(Tridi.__kron__(Ib))
		Laplace=Laplace.__add__(Laplacei)
	return Laplace

def Laplace_sparse(n,d):
    """ d-dimensional Laplace-Operator discretized with a finite difference stencil (-1,2,-1) in each direction """
    def index(i):
        idx = np.zeros(d, dtype=np.int64)
        for jdim in reversed(range(d)):
            idx[jdim], i = divmod(i, n**jdim)
        return idx
    N = n**d
    nnz = N * (1 + 2*d)
    row_ind = np.zeros(nnz, dtype=np.int64)
    col_ind = np.zeros(nnz, dtype=np.int64)
    data = np.zeros(nnz)
    off = 0
    for i in range(N):
        row_ind[off] = i
        col_ind[off] = i
        data[off] = 2*d
        off = off + 1
        idx = index(i)
        for idim in range(d):
            if idx[idim] != 0:
                j = i - n**idim
                row_ind[off] = i
                col_ind[off] = j
                data[off] = -1
                off = off + 1
            if idx[idim] != n-1:
                j = i + n**idim
                row_ind[off] = i
                col_ind[off] = j
                data[off] = -1
                off = off + 1
    A = scipy.sparse.csr_matrix((data, (row_ind, col_ind)), (N,N))
    return A

"""
Diskretisierung der Konvektion-Diffusion Gleichung mit D isotrop =1 und konstanter Geschwindigkeit alpha, Stroemung nur in x1 Richtung:
-\laplace c + alpha \cdot \Nabla c = \lambda c

stencil:
-\lablace -> (-1,2,-1)/h^2
\nabla -> (-1,1,0)/h  Backwards-diffquot

h=1/(n+1) gridsize
"""
def ConvDiffx1(n,d,alpha=1):
    dataL = np.array([-1*np.ones(n),2*np.ones(n),-1*np.ones(n)])*(n+1)*(n+1)
    diagsL = np.array([-1,0,1])
    AL = scipy.sparse.spdiags(dataL,diagsL,n,n).toarray()
    TridiL = tt.matrix(a=AL)
    ConvDiffL = TridiL.__kron__(tt.eye(n,d-1))
    for i in range(1,d):
    	Ia = tt.eye(n,i)
    	Ib = tt.eye(n,d-(i+1))
    	ConvDiffi = Ia.__kron__(TridiL.__kron__(Ib))
    	ConvDiffL=ConvDiffL.__add__(ConvDiffi)
    data = np.array([-1*np.ones(n),np.ones(n)])*(n+1)
    diags = np.array([-1,0])
    A = scipy.sparse.spdiags(data,diags,n,n).toarray()
    Tridi = tt.matrix(a=A)
    ConvDiffK = Tridi.__kron__(tt.eye(n,d-1))
    ConvDiffK = ConvDiffK.__mul__(alpha)
    ConvDiff = ConvDiffL.__add__(ConvDiffK)
    return ConvDiff
    
def ConvDiffx1_sparse(n,d,alpha=1):
    """ Konvektion-Diffusion als sparse Matrix """
    def index(i):
        idx = np.zeros(d, dtype=np.int64)
        for jdim in reversed(range(d)):
            idx[jdim], i = divmod(i, n**jdim)
        return idx
    N = n**d
    nnz = N * (1 + 2*d)
    row_ind = np.zeros(nnz, dtype=np.int64)
    col_ind = np.zeros(nnz, dtype=np.int64)
    data = np.zeros(nnz)
    off = 0
    for i in range(N):
        row_ind[off] = i
        col_ind[off] = i
        data[off] = 2*d*(n+1)*(n+1)+alpha*(n+1)
        off = off + 1
        idx = index(i)
        for idim in range(d):
            if idx[idim] != 0:
                j = i - n**idim
                row_ind[off] = i
                col_ind[off] = j
                data[off] = -(n+1)*(n+1)
                if idim==0:
                	data[off] = -(n+1)*(n+1)-alpha*(n+1)
                off = off + 1
            if idx[idim] != n-1:
                j = i + n**idim
                row_ind[off] = i
                col_ind[off] = j
                data[off] = -(n+1)*(n+1)
                off = off + 1
    A = scipy.sparse.csr_matrix((data, (row_ind, col_ind)), (N,N))
    return A

"""
Diskretisierung der Konvektion-Diffusion Gleichung mit D isotrop =1 und konstanter Geschwindigkeit alpha, Stroemung in conv Richtung:
-\laplace c + alpha \cdot \Nabla c = \lambda c

stencil:
-\lablace -> (-1,2,-1)/h^2
\nabla -> (-1,1,0)/h  Backwards-diffquot

h=1/(n+1) gridsize

input:
n: discretization in one dimension
d: number of dimensions
conv: array of the dimensions, in which we have convection (Default=in all directions)
alpha: constant velocity of convection (Default=1)
"""
def ConvDiff(n,d,conv=None,alpha=1):
    if conv==None:
        conv=range(1,d+1)
    dataL = np.array([-1*np.ones(n),2*np.ones(n),-1*np.ones(n)])*(n+1)*(n+1)
    diagsL = np.array([-1,0,1])
    AL = scipy.sparse.spdiags(dataL,diagsL,n,n).toarray()
    TridiL = tt.matrix(a=AL)
    ConvDiffL = TridiL.__kron__(tt.eye(n,d-1))
    for i in range(1,d):
    	Ia = tt.eye(n,i)
    	Ib = tt.eye(n,d-(i+1))
    	ConvDiffi = Ia.__kron__(TridiL.__kron__(Ib))
    	ConvDiffL=ConvDiffL.__add__(ConvDiffi)
    data = np.array([-1*np.ones(n),np.ones(n)])*(n+1)
    diags = np.array([-1,0])
    A = scipy.sparse.spdiags(data,diags,n,n).toarray()
    Tridi = tt.matrix(a=A)
    for i in conv:
    	if i==conv[0]:
    		ConvDiffK = tt.eye(n,i-1).__kron__(Tridi.__kron__(tt.eye(n,d-i)))
    	else:
    		ConvDiffi = tt.eye(n,i-1).__kron__(Tridi.__kron__(tt.eye(n,d-i)))
    		ConvDiffK = ConvDiffK.__add__(ConvDiffi)
    alpha2 = np.sqrt(pow(alpha,2)/len(conv))
    ConvDiffK = ConvDiffK.__mul__(alpha2)
    ConvDiff = ConvDiffL.__add__(ConvDiffK)
    return ConvDiff
    
def ConvDiff_sparse(n,d,conv=None,alpha=1):
    """ Konvektion-Diffusion als sparse Matrix """
    def index(i):
        idx = np.zeros(d, dtype=np.int64)
        for jdim in reversed(range(d)):
            idx[jdim], i = divmod(i, n**jdim)
        return idx
    if conv==None:
        conv=range(1,d+1)
    N = n**d
    nnz = N * (1 + 2*d)
    row_ind = np.zeros(nnz, dtype=np.int64)
    col_ind = np.zeros(nnz, dtype=np.int64)
    data = np.zeros(nnz)
    off = 0
    alpha2=np.sqrt(pow(alpha,2)/len(conv))
    convhelp = np.zeros(d)
    for i in conv:
    	convhelp[i-1]=1
    for i in range(N):
        row_ind[off] = i
        col_ind[off] = i
        data[off] = 2*d*(n+1)*(n+1)+alpha2*len(conv)*(n+1)
        off = off + 1
        idx = index(i)
        for idim in range(d):
            if idx[idim] != 0:
                j = i - n**idim
                row_ind[off] = i
                col_ind[off] = j
                data[off] = -(n+1)*(n+1)
                if convhelp[idim]==1: # hier muesste dies fuer alle idim in conv sein
                	data[off] = -(n+1)*(n+1)-alpha2*(n+1)
                off = off + 1
            if idx[idim] != n-1:
                j = i + n**idim
                row_ind[off] = i
                col_ind[off] = j
                data[off] = -(n+1)*(n+1)
                off = off + 1
    A = scipy.sparse.csr_matrix((data, (row_ind, col_ind)), (N,N))
    return A

#A=ConvDiff(2,3,alpha=0.5)
#B=ConvDiff_sparse(2,3,alpha=0.5)
#A_full=A.full()
#B_full=B.full()
#C=A_full-B
#print la.norm(C) #,C
#print A_full
#print B.todense()
#print B_full
