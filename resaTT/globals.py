global num_axpy
global num_scalpr
global time_axpy
global time_scalpr


def initialize():
	global num_axpy
	global num_scalpr
	global num_op
	global time_axpy
	global time_scalpr
	global time_op
	num_axpy=0
	num_scalpr=0
	num_op=0
	time_axpy=0
	time_scalpr=0
	time_op=0

def increment(value):
	global num_axpy
        global num_scalpr
	global num_op
        global time_axpy
        global time_scalpr
	global time_op

	if len(value)!=6:
		print("global value error!")
	num_axpy += value[0]
	num_scalpr += value[1]
	num_op += value[2]
	time_axpy += value[3]
	time_scalpr += value[4]
	time_op += value[5]

def output():
	global num_axpy
        global num_scalpr
	global num_op
        global time_axpy
        global time_scalpr
	global time_op

	print "num axpy: ",num_axpy
        print "time axpy: ",time_axpy
        print "num scalpr: ",num_scalpr
        print "time scalpr: ",time_scalpr
	print "num op: ",num_op
	print "time op: ",time_op
