import tt as tt
import numpy as np
import numpy.linalg as la
import tools as rtt
import time
import globals

'''
implementation of some gram-schmidt methods in the TT-Format

Input:
w: vector to orthogonalize against V
V: already orthogonal vectors
j: len(V)-1
eps: accuracy of truncations
ortho: some orthogonalization specification

Output:
vnew: orthogonalized w
h: entries for H
'''

def classical(w,V,j,eps,ortho='for'):
	h = np.zeros((j+2))
	if ortho=='for':
		order = range(j+1)
	elif ortho=='back':
		order = range(j,-1,-1)
	else:
		print "not implemented"
	for i in order:
		h[i] = tt.dot(w, V[i])
	for i in order:
		w = w + (-h[i]) * V[i]
		w = rtt.round(w,eps)
	h[j+1] = w.norm()
	vnew = w * (1 / h[j+1])
	return vnew, h

def modified(w,V,j,eps,ortho='for'):
	h = np.zeros((j+2))
	if ortho=='for':
		order = range(j+1)
	elif ortho=='back':
		order = range(j,-1,-1)
	else:
		print "not implemented"
	for i in order:
		t1=time.time()
		h[i] = tt.dot(w, V[i])
		t2=time.time()
		w = w + (-h[i]) * V[i]
		w = rtt.round(w, eps)
		t3=time.time()
		globals.increment([1,1,0,t3-t2,t2-t1,0])
	t1=time.time()
	h[j+1] = w.norm()
	t2=time.time()
	globals.increment([0,1,0,0,t2-t1,0])
	vnew = w * (1 / h[j+1])
	return vnew, h


def iterated_classical(w,V,j,eps,ortho='for'):
	h = np.zeros((j+2))
	g = np.zeros((j+1))
	if ortho=='for':
		order = range(j+1)
	elif ortho=='back':
		order = range(j,-1,-1)
	else:
		print "not implemented"
	for i in order:
		h[i] = tt.dot(w, V[i])
	for i in order:
		w = w + (-h[i]) * V[i]
		w = rtt.round(w,eps)
	for i in order:
		g[i] = tt.dot(w, V[i])
		h[i] = h[i] + g[i]
	for i in order:
		w = w + (-g[i]) * V[i]
		w = rtt.round(w,eps)		
	h[j+1] = w.norm()
	vnew = w * (1 / h[j+1])
	return vnew, h

def selective_iterated_modified(w,V,j,eps,ortho=3):
	h = np.zeros((j+2))
	g = np.zeros((j+1))
	for i in range(j+1):
		t1=time.time()
		g[i] = tt.dot(w, V[i])
		t2=time.time()
		globals.increment([0,1,0,0,t2-t1,0])
		h[i] = g[i]
	for it in range(ortho):
		order = np.argsort(-np.abs(g))
		for i in order:
			if np.abs(g)[i]<eps:
				break
			t1=time.time()
			tmp = tt.dot(w,V[i])
			t2=time.time()
			w = w + (-tmp) * V[i]
			w = rtt.round(w, eps)
			t3=time.time()
			globals.increment([1,1,0,t3-t2,t2-t1,0])
		for i in range(j+1):
			t1=time.time()
			g[i] = tt.dot(w, V[i])
			t2=time.time()
			globals.increment([0,1,0,0,t2-t1,0])
		if np.max(np.abs(g)) < eps:
			break
	t1=time.time()
	h[j+1] = w.norm()
	t2=time.time()
	globals.increment([0,1,0,0,t2-t1,0])
	vnew = w * (1 / h[j+1])
	return vnew, h

def selective_iterated_classical(w,V,j,eps,ortho=3):
	h = np.zeros((j+2))
	g = np.zeros((j+1))
	for it in range(ortho):
		for i in range(j+1):
			g[i] = tt.dot(w, V[i])
			h[i] = h[i] + g[i]
		if np.max(np.abs(g)) < eps:
			break
		order = np.argsort(-np.abs(g))
		for i in order:
			if np.abs(g)[i]<eps:
				break
			w = w + (-g[i]) * V[i]
			w = rtt.round(w,eps)
	h[j+1] = w.norm()
	vnew = w * (1 / h[j+1])
	return vnew, h

'''
with this method we can choose a gram-schmidt method through the modus parameter
so we can easy test different gs-methods in other algorithms
'''
def gs(w,V,j,eps,modus='m'):
	if modus=='m':
		vnew, h = modified(w,V,j,eps)
	elif modus=='c':
		vnew, h = classical(w,V,j,eps)
	elif modus=='ic':
		vnew, h = iterated_classical(w,V,j,eps)
	elif modus=='sim':
		vnew, h = selective_iterated_modified(w,V,j,eps)	
	elif modus=='sic':
		vnew, h = selective_iterated_classical(w,V,j,eps)
	else:
		print "not implemented"
	return vnew, h

