import tt as tt
import numpy as np
import numpy.linalg as la

'''
Creates TT-Tensor of all zeros

Input:
n: shape of output
d: dimension of output

Output:
c: zero-TT-Tensor
'''
def zeros(n, d=None):
	c = tt.vector()
	if d is None:
		c.n = np.array(n, dtype=np.int32)
		c.d = c.n.size
	else:
		c.n = np.array([n] * d, dtype=np.int32)
		c.d = d
	c.r = np.ones((c.d + 1,), dtype=np.int32)
	c.get_ps()
	c.core = np.zeros(c.ps[c.d] - 1)
	return c

'''
Transformation from a TT-Tensor to a Vector

Input:
TTv: TT-Tensor to transform

Output:
v: TTv as Vector
'''
def TT2Vec(TTv):
	v = np.reshape(TTv.full(),(np.prod(TTv.n),1),order='F')
	return v

'''
Transformation from a Vector to a TT-Tensor

Input:
v: Vector to transform
n: shape of the TT-Tensor
eps: accuracy to achive

Output:
vTT: v in TT-Format
'''
def Vec2TT(v,n,eps=1e-14):
	if la.norm(v)!=0:
		vTens = np.reshape(v,n,order='F')
		vTT = tt.tensor(a=vTens,eps=eps)
	else:
		vTT = zeros(n)
	return vTT

'''
Truncation of TT-tensor such that we get a zero-tensor if its norm is near zero

Input:
v: TT-tensor to truncate
eps: accuracy of truncation

Output:
v: truncated TT-tensor
'''
def round(v,eps,rmax=1000000):
	v = v.round(eps,rmax)
	if v.norm()<1e-16:
		v = zeros(v.n)
		#print 'v is zero'
	#print get_rmax(v)
	return v

'''
get maximal bonddimension

Input:
v: TT-tensor

Output:
maximal bonddimension of v
'''
def get_rmax(v):
	r = v.r
	return max(r)

'''
Computes complex conjugated of TT-vektor or complex conjugated transposed of TT-matrix

Input:
A: TT-vektor or TT-matrix

Output:
Aconj: complex conjugated transposed of A
'''
def conjt(A):
    if A.is_complex:
        Ai = A.imag()
	Ar = A.real()
        Aconj = Ar-Ai
        if hasattr(A,'T'):
            Aconj = Aconj.T()
        return Aconj
    else:
        return A

'''
Matrix-vector multiplication in TT format
'''
def matvec(A, x):
    # small wrapper without copying to make x look like a (n x 1) object of type tt.matrix
    class matrix:
        def __init__(self, vec):
            self.tt = vec
            self.n = vec.n
            self.m = vec.n/vec.n
        @property
        def is_complex(self):
            return self.tt.is_complex
    #mx = tt.matrix(x, n=x.n, m=x.n/x.n)
    Ax = A.__matmul__(matrix(x))
    return Ax.tt
