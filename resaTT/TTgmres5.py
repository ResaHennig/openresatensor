import tt as tt
import numpy as np
import numpy.linalg as la
import tools as rtt
import time
import SaveInFile
import gramschmidt as gs
import globals

'''
TT-GMRES with adjustable gram-schmidt method

GMRES linear systems solver based on TT techniques to solve A*x=b

Input:
A = A(x[, eps]): function that multiplies x by matrix.
b: right-hand side in TT-Format

Output:
x: solution in TT-Format

Options:
u_0: start solution in TT-Format
eps: accuracy to achive
restart: maximal number of iterations per restart
maxiter: maximal number of restarts
verb: verbosity of outputmessages
adapt: use 0-fixed / 1-adaptive truncation toleranz
fname: name of file to store data of computation
gsmod: chooses gram-schmidt method (Default:'m' (modified)) better:'ipm'
'''

def TTGMRES(A, b,u_0=None, eps=1E-6, restart=20, maxiter=None,verb=0,adapt=0,fname=None,gsmod='m'):
    globals.initialize()
    start= time.time()
    zero_start_vector = False
    if u_0 == None:
        u_0=rtt.zeros(b.n)
        zero_start_vector = True
    do_restart = True
    numrestarts = 0
    rmaxv = []
    rmaxx = []
    solit=[rtt.get_rmax(u_0)]
    convergence=[]
    while do_restart:
        if zero_start_vector:
            r0 = b
        else:
            t0=time.time()
            Au0 = A((-1) * u_0)
            t1=time.time()
            r0 = b + Au0
            r0 = rtt.round(r0,eps)
            t2=time.time()
            globals.increment([1,0,1,t2-t1,0,t1-t0])
        t1=time.time()
        beta = r0.norm()
        bnorm = b.norm()
        t2=time.time()
        globals.increment([0,2,0,0,t2-t1,0])
        curr_beta = beta
        convergence.append(curr_beta)
        if verb:
            print("/ Initial  residual  norm: %lf; mean rank:" % beta, r0.rmean())
        m = restart
        V = np.zeros(m + 1, dtype=object)  # Krylov basis
        V[0] = r0 * (1.0 / beta)
        rmaxv.append(rtt.get_rmax(V[0]))
        H = np.mat(np.zeros((m + 1, m), dtype=np.complex128, order='F'))
        j = 0
        while j < m and curr_beta / beta > eps and curr_beta / bnorm > eps:	#changed condition to ensure delta<1
            if adapt==1:
                delta = eps / (curr_beta *1.2 *m / beta)
            else:
                delta = eps
            # print("Calculating new Krylov vector")
            t0=time.time()
            w = A(V[j], delta)
            t1=time.time()
            globals.increment([0,0,1,0,0,t1-t0])
            vnew, h = gs.gs(w,V,j,delta,modus=gsmod)
            H[:j + 2, j] = h.reshape((len(h),1))[:]
            V[j + 1] = vnew
            rmaxv.append(rtt.get_rmax(V[j+1]))
            Hj = H[:j + 2, :j + 1]
            betae = np.zeros(j + 2, dtype=np.complex128)
            betae[0] = beta
			
            # solving Hj * y = beta e_1
            y, curr_beta, rank, s = np.linalg.lstsq(Hj, betae,rcond=-1)
            curr_beta = np.sqrt(curr_beta[0])
            convergence.append(curr_beta)
            if verb:
                print("|% 3d. LSTSQ residual norm:" % (j + 1), curr_beta)
            j += 1
            #x = u_0
            #for i in range(j):
            #	x = x + V[i] * y[i]
            #	x = rtt.round(x, eps)
            #solit.append(rtt.get_rmax(x))
            #end = time.time()
            #real_res = (b-A(x)).norm()
            #zeit = end-start
            #SaveInFile.SaveGmres(fname,adapt, mtest,roundlater, ortho,rmaxv,rmaxx, solit,convergence, real_res,zeit,gs=gsmod,maxiter=maxiter)
        x = u_0
        rmaxx.append(rtt.get_rmax(x))
        for i in range(j):
            t1=time.time()
            x = x + V[i] * y[i]
            x = rtt.round(x, eps)
            t2=time.time()
            globals.increment([1,0,0,t2-t1,0,0])
            rmaxx.append(rtt.get_rmax(x))
        print("rmax sol",rtt.get_rmax(x))
        if verb:
            print("\\ Solution mean rank:", x.rmean())
        u_0 = x
        zero_start_vector = False
        numrestarts = numrestarts +1
        if maxiter == None:
            do_restart = (curr_beta / bnorm > eps)
        else:
            if numrestarts<maxiter:
                do_restart = (curr_beta / bnorm > eps)
            else:
                do_restart = False
    end = time.time()
    if fname!=None:
        real_res = (b-A(x)).norm()
        zeit = end-start
        SaveInFile.SaveGmres(fname,adapt,rmaxv,rmaxx, solit,convergence, real_res,zeit,gs=gsmod,maxiter=maxiter)
    if verb>0:
        globals.output()
        print "TTGMRES5 runtime:", end-start
    return x
