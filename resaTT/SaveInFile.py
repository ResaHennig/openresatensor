import tt as tt
import numpy as np
import numpy.linalg as la
import tools as rtt
import time

'''
This routine saves data of the GMRES computation in a file
'''
def SaveGmres(fname,adapt,rmaxv,rmaxx,solit,convergence,real_res,zeit,gs='m',maxiter=1):
	f = open(fname,"a")
	f.write("\n# adapt="+str(adapt)+str(gs)+"gs\n")
	f.write("rmaxv="+str(rmaxv)+"\n")
	f.write("rmaxx="+str(rmaxx)+"\n")
	f.write("solit="+str(solit)+"\n")
	f.write("LSTSQ_res="+str(convergence)+"\n")
	f.write("real_residual="+str(real_res)+"\n")
	f.write("zeit="+str(zeit)+"\n")
	if adapt==0:
		if maxiter==1:
			methodname = "nonadaptiv"
			marker = "marker=\'o\',markerfacecolor=\'none\',markevery=5,"
		else:
			methodname = "nonadaptiv restarted"
			marker = "marker=\'D\',markerfacecolor=\'none\',markevery=5,"
	elif adapt==1:
		if maxiter==1:
			methodname = "adaptiv"
			marker = "marker=\'x\',markevery=5,"
		else:
			methodname = "adaptiv restarted"
			marker = "marker=\'^\',markerfacecolor=\'none\',markevery=5,"
	
	f.write("p=plt.plot(rmaxv,"+marker+"label=\'krylovvector "+methodname+"\')\n")
	f.write("#plt.plot(rmaxx,\'--\',"+marker+"label=\'x "+methodname+"\',color=p[0].get_color())\n")
	f.write("plt.plot(solit,\':\',"+marker+"label=\'solution "+methodname+"\',color=p[0].get_color())\n")
	f.write("#plt.text(len(rmaxv),rmaxv[len(rmaxv)-1],str(zeit),color=p[0].get_color())\n")
	f.close()
