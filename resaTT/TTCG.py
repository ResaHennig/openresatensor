import tt as tt
import numpy as np
import numpy.linalg as la
import tools as rtt
import time

'''
TT-CG

CGhen linear systems solver based on TT techniques to solve A*x=b

Input:
A = A(x[, eps]): function that multiplies x by matrix.
b: right-hand side in TT-Format

Output:
x: solution in TT-Format

Options:
u_0: start solution in TT-Format
eps: accuracy to achive
maxiter: maximal number of iterations
adapt: use adapted truncation toleranz
fname: filename to save data
'''
def TTCG(A, b,u_0=None, eps=1E-6, maxiter=100,adapt=0,fname=None):
	start= time.time()			
	zero_start_vector = False
	if u_0 == None:
		u_0=rtt.zeros(b.n)
		zero_start_vector = True	
	if zero_start_vector:
		r = b
	else:
		r = b + A((-1) * u_0)
		r = rtt.round(r,eps)	
	p = r
	x = u_0
	r0norm = r.norm()
	rnorm = r.norm()
	
	rmaxp = [rtt.get_rmax(p)]
	rmaxx = [rtt.get_rmax(x)]
	rmaxr = [rtt.get_rmax(r)]
	convergence=[rnorm]
	j = 0
	while j < maxiter and (rnorm / r0norm) > eps:
		if adapt==1:
			delta = eps / (rnorm *1.2*m/ r0norm) 
		else:
			delta = eps			
		q = A(p,delta)
		alpha = rnorm*rnorm / tt.dot(q,p)		
		x = x + alpha*p
		x = rtt.round(x,eps)
		r = r - alpha*q
		r = rtt.round(r,delta)
		rnewnorm = r.norm()
		beta = rnewnorm*rnewnorm / (rnorm*rnorm)
		p = r + beta*p
		p = rtt.round(p,delta)
		rnorm = rnewnorm
		
		rmaxp.append(rtt.get_rmax(p))
		rmaxx.append(rtt.get_rmax(x))
		rmaxr.append(rtt.get_rmax(r))
		convergence.append(rnorm)
		j += 1
	end = time.time()
	if fname!=None:
		f = open(fname,"a")
		f.write("\n# adapt="+str(adapt)+"\n")
		f.write("rmaxp="+str(rmaxp)+"\n")
		f.write("rmaxx="+str(rmaxx)+"\n")
		f.write("rmaxr="+str(rmaxr)+"\n")
		f.write("norm_r="+str(convergence)+"\n")
		f.write("zeit="+str(end-start)+"\n")
		f.write("real_residual="+str((b-A(x)).norm())+"\n")
		f.write("p=plt.plot(rmaxp,label=\'p: adapt="+str(adapt)+"\')\n")
		f.write("plt.plot(rmaxx,\':\',label=\'x: adapt="+str(adapt)+"\',color=p[0].get_color())\n")
		f.write("plt.plot(rmaxr,\'--\',label=\'r: adapt="+str(adapt)+"\',color=p[0].get_color())\n")
		f.write("plt.text(len(rmaxp),rmaxp[len(rmaxp)-1],str(zeit),color=p[0].get_color())\n")
		f.close()
	return x
