import tt as tt
import numpy as np
import numpy.linalg as la
import tools as rtt

'''
TT-GMRES

GMRES linear systems solver based on TT techniques to solve A*x=b

Input:
A = A(x[, eps]): function that multiplies x by matrix.
b: right-hand side in TT-Format

Output:
x: solution in TT-Format

Options:
u_0: start solution in TT-Format
eps: accuracy to achive
restart: maximal number of iterations per restart
maxiter: maximal number of restarts
verb: verbosity of outputmessages
adapt: use adapted truncation toleranz
'''
def TTGMRES(A, b,u_0=None, eps=1E-6, restart=20, maxiter=None,verb=0,adapt=False):
	zero_start_vector = False
	if u_0 == None:
		u_0=rtt.zeros(b.n)
		zero_start_vector = True
	do_restart = True
	numrestarts = 0
	while do_restart:
		if zero_start_vector:
		    r0 = b
		else:
		    r0 = b + A((-1) * u_0)
		    r0 = rtt.round(r0,eps)
		beta = r0.norm()
		bnorm = b.norm()
		curr_beta = beta
		if verb:
			print("/ Initial  residual  norm: %lf; mean rank:" % beta, r0.rmean())
		m = restart
		V = np.zeros(m + 1, dtype=object)  # Krylov basis
		V[0] = r0 * (1.0 / beta)
		H = np.mat(np.zeros((m + 1, m), dtype=np.complex128, order='F'))
		j = 0
		while j < m and curr_beta / beta > eps:	#changed condition to ensure delta<1
			if adapt:
				delta = eps / (curr_beta / beta)
			else:
				delta = eps
			# print("Calculating new Krylov vector")
			w = A(V[j], delta)
			for i in range(j + 1):
				H[i, j] = tt.dot(w, V[i])
				w = w + (-H[i, j]) * V[i]
				w = rtt.round(w, delta)
			if verb > 1:
				print("|% 3d. New Krylov vector mean rank:" % (j + 1), w.rmean())
			H[j + 1, j] = w.norm()
			V[j + 1] = w * (1 / H[j + 1, j])
			Hj = H[:j + 2, :j + 1]
			betae = np.zeros(j + 2, dtype=np.complex128)
			betae[0] = beta
			
			# solving Hj * y = beta e_1
			y, curr_beta, rank, s = np.linalg.lstsq(Hj, betae,rcond=-1)
			curr_beta = np.sqrt(curr_beta[0])
			if verb:
				print("|% 3d. LSTSQ residual norm:" % (j + 1), curr_beta)
			j += 1
		x = u_0
		for i in range(j):
			x = x + V[i] * y[i]
			x = rtt.round(x, eps)
		if verb:
			print("\\ Solution mean rank:", x.rmean())
		u_0 = x
		numrestarts = numrestarts +1
		zero_start_vector = False
		if maxiter == None:
			do_restart = (curr_beta / bnorm > eps)
		else:
			if numrestarts<maxiter:
				do_restart = (curr_beta / bnorm > eps)
			else:
				do_restart = False
	return x
