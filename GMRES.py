import numpy as np
import numpy.linalg as la

'''
GMRES

GMRES linear systems solver to solve A*x=b

Input:
A = A(x): linearOperator that multiplies x by matrix.
b: right-hand side

Output:
x: solution

Options:
u_0: start solution
eps: accuracy to achive
restart: maximal number of iterations per restart
maxiter: maximal number of restarts
verb: verbosity of outputmessages
'''
def gmres(A, b,u_0=None, eps=1E-6, restart=20, maxiter=None,verb=0):
	if u_0 == None:
		u_0=np.zeros((len(b),1))
	do_restart = True
	numrestarts = 0
	while do_restart:
		r0 = b + A.matvec((-1) * u_0).reshape((len(b),1))
		beta = la.norm(r0)
		bnorm = la.norm(b)
		curr_beta = beta
		if verb:
			print("/ Initial  residual  norm: %lf; mean rank:" % beta, r0.rmean())
		m = restart
		V = np.zeros(m + 1, dtype=object)  # Krylov basis
		V[0] = r0 * (1.0 / beta)
		H = np.mat(np.zeros((m + 1, m), dtype=b.dtype, order='F'))
		j = 0
		while j < m and curr_beta / beta > eps:
			# print("Calculating new Krylov vector")
			w = A.matvec(V[j]).reshape((len(b),1))
			for i in range(j + 1):
				H[i, j] = np.dot(np.transpose(w),V[i]) #Look here!
				w = w + (-H[i, j]) * V[i]

			if verb > 1:
				print("|% 3d. New Krylov vector mean rank:" % (j + 1), w.rmean())
			H[j + 1, j] = la.norm(w)
			V[j + 1] = w * (1 / H[j + 1, j])
			Hj = H[:j + 2, :j + 1]
			betae = np.zeros(j + 2, dtype=b.dtype)
			betae[0] = beta
			
			# solving Hj * y = beta e_1
			y, curr_beta, rank, s = np.linalg.lstsq(Hj, betae)
			curr_beta = np.sqrt(curr_beta[0])
			if verb:
				print("|% 3d. LSTSQ residual norm:" % (j + 1), curr_beta)
			j += 1
		x = u_0
		for i in range(j):
			x = x + V[i] * y[i]
		if verb:
			print("\\ Solution mean rank:", x.rmean())
		u_0 = x
		numrestarts = numrestarts +1
		if maxiter == None:
			do_restart = (curr_beta / bnorm > eps)
		else:
			if numrestarts<maxiter:
				do_restart = (curr_beta / bnorm > eps)
			else:
				do_restart = False
	return x
