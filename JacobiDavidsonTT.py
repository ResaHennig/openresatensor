# -*- coding: utf-8 -*- 

import numpy as np
import numpy.linalg as la
import scipy.linalg as sl
import scipy.sparse.linalg as ssl
import scipy.sparse as ss
import scipy.io as sio
import tt as tt
import Benchmark.spinchain as spin
import Benchmark.Laplace as laplace
import resaTT.tools as rtt
import matplotlib.pyplot as plt
import time
from Plotting import JadaColorPlot
from resaTT.TTgmres import *
import tt.eigb
import resatools as rt

global matvecs
matvecs = 0

'''Berechnet gerundet (I-QQ*)(A-r*I)(I-QQ*)x fuer A,Q,x in TT-Format, r ein Skalar'''
def CorrFunc(A,x,Q,r,eps=1e-14):
	global matvecs
	matvecs = matvecs + 1
	x = rtt.round(x,eps)
	#print 'x rmax', rtt.get_rmax(x)
	sol = rtt.matvec(A,x)
	sol = rtt.round(sol,eps)
	sol = sol - r*x
	sol = rtt.round(sol,eps)
	y = tt.dot(rtt.conjt(Q),sol)
	sol = sol-Q*y
	sol=rtt.round(sol,eps)
	#print 'sol rmax', rtt.get_rmax(sol)
	return sol

'''
JacobiDavidson Solver for Matrix in TT-Format.
Outer Iteration computed with full vectors
Inner Iteration computed in TT-Format

Computes an Eigenpair Q,R of a Matrix A in TT-Format

Input:
A: Matrix (None for fullTT case)
ATT: Matrix in TT-Format
tTT: Startvector in TT-Format
epsilon: accuracy to achive
maxIter: maximal number of outer Iterations

Output:
(Q,R): (eigenvector,eigenvalue) if not converged: ((0,...,0),0)
Attention: The Output is not in TT-Format!


Options:
verb: verbosity of output (Default =0)
		0 : runtime of Jada and runtime of parts of Jada
		1 : residual in each outer iteration + output of verb=0
		2 : timinginformation in each outer iteration + output of verb=1
		3+: some orthogonalization-test outputs + output of verb=2
gmres_tol: tolerance for the TTgmres (if adapt=True we use residuum*gmres_tol as tolerance for TTgmres)
adapt: set True to use an adapted tolerance in TTgmres
fullTT: by default TT-Jada uses the full Matrix in the outer iteration and uses ATT only in the inner iteration. Set fullTT to True to use ATT also in the outer iteration
symm: set to False if problem not symmetric
save: if True adds aditional timeinformations to the Output(Q,R,runtime,gmrestime,outertime)
arnoldiIter: sets number of arnoldi starting steps
'''
def JacobiDavidson (A,ATT, tTT, epsilon, maxIter,verb=0,gmres_tol=1e-6,adapt=False, fullTT=False, symm=True, save=False, arnoldiIter=0):
	print('JacobiDavidsonTT')
	'''initialisation'''
	start=time.time()
	t_n = tTT.n
	dim = np.prod(t_n)
	t = np.reshape(tTT.full(),(dim,1),order='F')
	H = np.zeros((0,0))
	W = np.zeros((dim,0))
	WA = np.zeros((dim,0))
	plotres = np.zeros((0))
	eigs = np.zeros((0))
	times = np.zeros((0))
	k = 0
	gmreszeit=0
	outerzeit=0
	rest=0
	tinit = time.time()-start
	print('inittime: {:5.6f}s'.format(tinit))

	'''outer iteration'''
	for nIter in range(1,maxIter+1):
		'''orthogonalize new searchdirection to searchspace'''
		itstart = time.time()
		v = t - np.dot(W,np.dot(rt.conjt(W),t))
		while la.norm(v) < 1e-16:
			print "random direction needed"
			t = np.random.rand(dim,1)
			v = t - np.dot(W,np.dot(rt.conjt(W),t))
		v = v /la.norm(v)
		v = v - np.dot(W,np.dot(rt.conjt(W),v))
		v = v/la.norm(v)
		
		'''update projection of A onto searchspace'''
		global matvecs
		if fullTT:
			vTT = rtt.Vec2TT(v,t_n)
			vATT = rtt.matvec(ATT,vTT)
			matvecs = matvecs+1
			vA = rtt.TT2Vec(vATT)
		else:
			vA = A.dot(v)
		H = np.append(H,np.dot(rt.conjt(W),vA).reshape((nIter-1,1)),axis=1)
		H = np.append(H,np.append(np.dot(rt.conjt(v),WA).reshape((1,nIter-1)),np.dot(rt.conjt(v),vA).reshape((1,1)),axis=1), axis=0)
		W = np.append(W,v,axis=1)
		WA = np.append(WA,vA,axis=1)
		
		'''Compute Schur decomposition H QH = QH RH'''
		if symm:
			#symmetric case
			(RH,QH) = la.eigh(H)
			R = RH[0]
			Q = np.dot(W,QH[:,0].reshape((nIter,1)))
			QA = np.dot(WA,QH[:,0].reshape((nIter,1)))
		else:
			#non-symmetric case
			(RH,QH) = la.eig(H) #eigenvalues not sorted
			#print RH
			ind = np.argmin(RH)
			R = RH[ind]
			Q = np.dot(W,QH[:,ind].reshape((nIter,1)))
			QA = np.dot(WA,QH[:,ind].reshape((nIter,1)))
		eigs = np.append(eigs,R)

		'''Compute Residual and check tolerance'''
		QTT = rtt.Vec2TT(Q,t_n,eps=epsilon*1e-2)
		if verb>0:
			sol = rtt.Vec2TT(Q,t_n)
			print "Bonddimension sol",rtt.get_rmax(sol)
			sol=rtt.round(sol,1e-12)
			print "rounded bonddimension",rtt.get_rmax(sol)
		QTT = rtt.round(QTT,epsilon*1e-2) #QTT.round(epsilon*1e-2)
		QATT = rtt.Vec2TT(QA,t_n,eps=epsilon*1e-2)
		QATT = rtt.round(QATT,epsilon*1e-2) #QATT.round(epsilon*1e-2)
		resTT = -(QATT-QTT*R)
		resTT = rtt.round(resTT,epsilon*1e-2) #resTT.round(epsilon*1e-2)
		normres = resTT.norm()
		#normres = la.norm(QA-Q*R)
		plotres = np.append(plotres,normres)
		t1 = time.time()-itstart
		outerzeit = outerzeit+t1
		
		if verb > 0:
			itend = time.time()
			print "Iteration",nIter,":"
			print "res", normres
			print "eig",R
			print('outerzeit: {:5.3f}s'.format(outerzeit))
			print('gmreszeit: {:5.3f}s'.format(gmreszeit))
			print('restzeit: {:5.3f}s'.format(rest))
			print('zeit: {:5.3f}s'.format(itend-start))
			print "matvecs",matvecs
			print "bond qtt",rtt.get_rmax(QTT)
			print "bond res",rtt.get_rmax(resTT)
		if verb > 1:
			print('t1: {:5.6f}s'.format(t1))
		if normres < epsilon:
			itend = time.time()
			times = np.append(times,itend-itstart)
			ende = time.time()
			print('outerzeit: {:5.3f}s'.format(outerzeit))
			print('gmreszeit: {:5.3f}s'.format(gmreszeit))
			print('restzeit: {:5.3f}s'.format(rest))
			print('zeit: {:5.3f}s'.format(ende-start))
			print "matvecs",matvecs
			matvecs=0
			#print eigs
			#JadaColorPlot(range(1,nIter+1),plotres,eigs,'TT Jacobi-Davidson')
			#plt.plot(range(1,nIter+1),times[0:nIter])
			#plt.show()
			#return (Q,R,plotres,ende-start)
			if save:
				return (Q,R,ende-start,gmreszeit,outerzeit)
			else:
				return (Q,R)

		if nIter <= arnoldiIter:
			if verb > 0:
				print "Iteration",nIter,":", "generating initial subspace with Arnoldi"
			t = vA - np.dot(Q,np.dot(rt.conjt(Q),vA))
			continue

		'''Approximativly solve (I-QQ*)(A-rii*I)(I-QQ*)t=-res'''
		y = tt.dot(rtt.conjt(QTT),resTT)
		resTT = resTT-QTT*y
		resTT = rtt.round(resTT,epsilon*1e-2) #resTT.round(epsilon*1e-2)
		#print 'Qtt rmax', rtt.get_rmax(QTT)
		#print 'restt rmax', rtt.get_rmax(resTT)
		t2 = time.time()-t1-itstart
		if adapt:
			tTT = TTGMRES((lambda x,eps=1e-14:CorrFunc(ATT,x,QTT,R,eps=eps)),resTT,verb=0,eps=normres*gmres_tol,maxiter=1,restart=10)
		else:
			tTT = TTGMRES((lambda x,eps=1e-14:CorrFunc(ATT,x,QTT,R,eps=eps)),resTT,verb=0,eps=gmres_tol,maxiter=1,restart=10)
		t3 = time.time()-t2-t1-itstart
		gmreszeit=gmreszeit+t3
		t = rtt.TT2Vec(tTT)
		#print "Bonddimension t",rtt.get_rmax(tTT)
		t = t - np.dot(Q,np.dot(rt.conjt(Q),t))
		t4 = time.time()-t2-t1-itstart-t3
		rest=rest+t2+t4
		if verb > 1:
			print('t2: {:5.6f}s'.format(t2))
			print('t3: {:5.6f}s'.format(t3))
			print('t4: {:5.6f}s'.format(t4))
		if verb > 2:
			print "current eigenvalue R = ", R
			print "Orthogonality of W: max(W*W-I) = ", np.amax(np.dot(rt.conjt(W),W)-np.identity(nIter))
			print "Orthogonality of Q: Q*Q = ", np.dot(rt.conjt(Q),Q)
			print "Orthogonality of t to Q: Q*t = ", np.dot(rt.conjt(Q),t)
		itend = time.time()
		times = np.append(times,itend-itstart)
		if verb > 0:
			print "----------------------------------------------------------"
	'''not converged'''
	ende = time.time()
	print('outerzeit: {:5.3f}s'.format(outerzeit))
	print('gmreszeit: {:5.3f}s'.format(gmreszeit))
	print('restzeit: {:5.3f}s'.format(rest))
	print('zeit: {:5.3f}s'.format(ende-start))
	#JadaColorPlot(range(1,nIter+1),plotres,eigs,'TT Jacobi-Davidson')
	#JadaColorPlot(range(1,nIter+1),plotres,eigs,'SpinOpB $L=14$ TT-Jada')
	#plt.plot(range(1,maxIter+1),times[0:maxIter])
	#plt.show()
	if nIter == maxIter:
		print "Jacobi-Davidson not converged!"
		matvecs=0
		return (np.zeros((dim,1)),0)
		
'''
JacobiDavidson Solver for Matrix in TT-Format
Computation completely in TT-Format

Computes an Eigenpair Q,R of a Matrix A in TT-Format

Input:
ATT: Matrix in TT-Format
tTT: Startvector in TT-Format
epsilon: accuracy to achive
maxIter: maximal number of outer Iterations

Output:
(Q,R): (eigenvector,eigenvalue) if not converged: ((0,...,0),0)
Attention: The Output is in TT-Format!


Options:
verb: verbosity of output (Default =0)
		0 : runtime of Jada and runtime of parts of Jada
		1 : residual in each outer iteration + output of verb=0
		2 : timinginformation in each outer iteration + output of verb=1
		3+: some orthogonalization-test outputs + output of verb=2
gmres_tol: tolerance for the TTgmres (if adapt=True we use residuum*gmres_tol as tolerance for TTgmres)
adapt: set True to use an adapted tolerance in TTgmres
save: if True adds aditional timeinformations to the Output(Q,R,runtime,gmrestime,outertime)
arnoldiIter: sets number of arnoldi starting steps
'''
def TTJacobiDavidson (ATT, tTT, epsilon, maxIter,verb=0,gmres_tol=1e-6,adapt=False, save=False, arnoldiIter=0):
	print('TTJacobiDavidsonTT')
	'''initialisation'''
	start=time.time()
	t_n = tTT.n
	dim = np.prod(t_n)
	H = np.zeros((0,0))
	W = []
	WA = []
	plotres = np.zeros((0))
	eigs = np.zeros((0))
	times = np.zeros((0))
	k = 0
	gmreszeit=0
	outerzeit=0
	rest=0
	tinit = time.time()-start
	print('inittime: {:5.6f}s'.format(tinit))

	'''outer iteration'''
	for nIter in range(1,maxIter+1):
		'''orthogonalize new searchdirection to searchspace'''
		itstart = time.time()
		v = tTT
		for i in range(0,nIter-1):
			y = tt.dot(W[i],tTT)
			v = v - y*W[i]
			v = rtt.round(v,epsilon*1e-2)
		v = v * (1/v.norm())
		if v.norm() < 1e-16:
			print "random direction needed"
			#t = np.random.rand(dim,1)
			#v = t - np.dot(W,np.dot(rt.conjt(W),t))
			print "not implemented"
			break
		for i in range(0,nIter-1):
			y = tt.dot(W[i],v)
			v = v - y*W[i]
			v = rtt.round(v,epsilon*1e-2)
		v = v * (1/v.norm())
		
		'''update projection of A onto searchspace'''
		global matvecs
		vA = rtt.matvec(ATT,v)
		matvecs = matvecs+1
		
		H = np.append(H,np.zeros((nIter-1,1)),axis=1)
		for i in range(0,nIter-1):
			H[i,nIter-1] = tt.dot(rtt.conjt(vA),W[i])
		H = np.append(H,np.zeros((1,nIter)),axis=0)
		for i in range(0,nIter-1):
			H[nIter-1,i] = tt.dot(rtt.conjt(v),WA[i])
		H[nIter-1,nIter-1]= tt.dot(rtt.conjt(v),vA)
		W = np.append(W,v)
		WA = np.append(WA,vA)
		
		'''Compute Schur decomposition H QH = QH RH'''
		(RH,QH) = la.eig(H) #eigenvalues not sorted
		ind = np.argmin(RH)
		R = RH[ind]
		Q = rtt.zeros(t_n)
		QA = rtt.zeros(t_n)
		for i in range(0,nIter):
			Q = Q + W[i]*QH[i,ind]
			QA = QA + WA[i]*QH[i,ind]
			Q=rtt.round(Q,epsilon*1e-2)
			QA = rtt.round(QA,epsilon*1e-2)
		eigs = np.append(eigs,R)

		'''Compute Residual and check tolerance'''
		res = (QA-Q*R)
		res = rtt.round(res,epsilon*1e-2)
		normres = res.norm()
		
		plotres = np.append(plotres,normres)
		t1 = time.time()-itstart
		outerzeit = outerzeit+t1
		
		if verb > 0:
			itend = time.time()
			print "Iteration",nIter,":"
			print "res", normres
			print "eig",R
			print('outerzeit: {:5.3f}s'.format(outerzeit))
			print('gmreszeit: {:5.3f}s'.format(gmreszeit))
			print('restzeit: {:5.3f}s'.format(rest))
			print('zeit: {:5.3f}s'.format(itend-start))
			print "matvecs",matvecs
			print "bond qtt",rtt.get_rmax(Q)
			print "bond res",rtt.get_rmax(res)
		if verb > 1:
			print('t1: {:5.6f}s'.format(t1))
		if normres < epsilon:
			itend = time.time()
			times = np.append(times,itend-itstart)
			ende = time.time()
			print('outerzeit: {:5.3f}s'.format(outerzeit))
			print('gmreszeit: {:5.3f}s'.format(gmreszeit))
			print('restzeit: {:5.3f}s'.format(rest))
			print('zeit: {:5.3f}s'.format(ende-start))
			print "matvecs",matvecs
			matvecs=0
			if save:
				return (Q,R,ende-start,gmreszeit,outerzeit)
			else:
				return (Q,R)

		if nIter <= arnoldiIter:
			if verb > 0:
				print "Iteration",nIter,":", "generating initial subspace with Arnoldi"
			tTT = vA - Q*tt.dot(rtt.conjt(Q),vA)
			tTT = rtt.round(tTT,epsilon*1e-2)
			continue

		'''Approximativly solve (I-QQ*)(A-rii*I)(I-QQ*)t=-res'''
		res = res - Q*tt.dot(rtt.conjt(Q),res)
		res = rtt.round(res,epsilon*1e-2)
		
		t2 = time.time()-t1-itstart
		if adapt:
			tTT = TTGMRES((lambda x,eps=1e-14:CorrFunc(ATT,x,Q,R,eps=eps)),res,verb=0,eps=normres*gmres_tol,maxiter=1,restart=10,adapt=True)
		else:
			tTT = TTGMRES((lambda x,eps=1e-14:CorrFunc(ATT,x,Q,R,eps=eps)),res,verb=0,eps=gmres_tol,maxiter=1,restart=10)
		t3 = time.time()-t2-t1-itstart
		gmreszeit=gmreszeit+t3
		
		#print "Bonddimension t",rtt.get_rmax(tTT)
		tTT = tTT - Q*tt.dot(rtt.conjt(Q),tTT)
		tTT = rtt.round(tTT,epsilon*1e-2)
		
		t4 = time.time()-t2-t1-itstart-t3
		rest=rest+t2+t4
		if verb > 1:
			print('t2: {:5.6f}s'.format(t2))
			print('t3: {:5.6f}s'.format(t3))
			print('t4: {:5.6f}s'.format(t4))
		
		itend = time.time()
		times = np.append(times,itend-itstart)
		if verb > 0:
			print "----------------------------------------------------------"
	'''not converged'''
	ende = time.time()
	print('outerzeit: {:5.3f}s'.format(outerzeit))
	print('gmreszeit: {:5.3f}s'.format(gmreszeit))
	print('restzeit: {:5.3f}s'.format(rest))
	print('zeit: {:5.3f}s'.format(ende-start))
	if nIter == maxIter:
		print "Jacobi-Davidson not converged!"
		matvecs=0
		return (np.zeros((dim,1)),0)
