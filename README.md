# ResaTensor
***

This Repository includes all the codes from my masterthesis "Low-Rank-Tensor-Techniken im Jacobi-Davidson-Eigenwertl�ser" - Rebekka-Sarah Hennig 
and additional code from my work on the paper "Rank-bounding techniques in tensor-train Krylov methods for problems with displacement structure" - Melven R�hrig-Z�llner, Rebekka-Sarah Hennig, Jonas Thies and Achim Basermann.

In my masterthesis I worked on including the Tensor-Train Format into the Jacobi-Davidson Method by using the TT-GMRES Algorithm.
I than compared the new TT-Jacobi-Davidson Method with the old one and looked also at the eigb Algorithm of ttpy.

In the Paper we focused on the TT-GMRES method and analyzed the effect of a displacement structure on the ranks throughout the computation.

For the visualization of operations and tensors in the TT-Format I wrote the TTplot.py file, which makes plotting of algorithms in TT-format easier.
It is enough for the pictures in my thesis but does not include everything you can asked for in such a visualization.

## Structure
***
The main routines are in JacobiDavidson.py and JacobiDavidsonTT.py.

In the resaTT folder there are some additional routines that we needed, to make the (TT-)Jada codes work or to simplify them.

In the Benchmark folder we have files to create the Benchmarks for the Spinproblem, Laplace and Convection-Diffusion Operator.

In the spin folder there are matrixmarket versions of the spinoperator.

The remainders are mostly for testing.

## Installation
***
Since my code is based on the ttpy Python-Package, you have to install this Package, to make the code run.
For me the following steps worked fine when installing it on my VM:
```
$ sudo apt-get install gfortran
$ # download Anaconda installer
$ bash Downloads/Anaconda2-2019.10-Linux-x86-64.sh
$ conda install numpy cython
$ pip install ttpy
```
For my own Repository I needed to add the following
```
$ pip install scipy matplotlib
$ sudo apt install git
$ git clone git@bitbucket.org:ResaHennig/resatensor.git
```