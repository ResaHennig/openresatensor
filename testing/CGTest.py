import sys
sys.path.append('../')

import tt as tt
import scipy.io as sio
import resaTT.TTCG as TTCG
import Benchmark.Laplace as laplace
import resaTT.tools as rtt
import time
import matplotlib.pyplot as plt
import numpy as np

def Test(A,x,eps=1e-14):
    sol=rtt.matvec(A,x)
    return rtt.round(sol,eps)

n = [20,20,20,20,20,20]
d = [6]
maxiter = [90]
case = ['random','La','La','La','La','La','La']
epsilon = [1e-8,1e-6,1e-6,1e-8,1e-8,1e-8]

conv = None
alpha = 0.1
verb = 0

for i in range(len(d)):
    if case[i] == 'La':
        print "create Laplace n=",n[i],"d=",d[i]
        Att = laplace.Laplace(n[i],d[i])
        fname = "cgtest/CGTT_Laplace"+"_n"+str(n[i])+"_d"+str(d[i])+"_m"+str(maxiter[i])+"_eps"+str(epsilon[i])+".py"

    elif case[i] == 'CD':
        print "create TTConvDiff n=",n[i],"d=", d[i]
        Att = laplace.ConvDiff(n[i],d[i],conv=conv,alpha=alpha)
        fname = "cgtest/CGTT_ConvDiffNone01"+"_n"+str(n[i])+"_d"+str(d[i])+"_m"+str(maxiter[i])+"_eps"+str(epsilon[i])+".py"
    elif case[i] =='random':
        print "create random operator n=",n[i],"d=", d[i]
        temp = tt.rand(n[i]*n[i],d=d[i],r=2)
        Att = tt.matrix(a=temp)
        Att = Att*(1/Att.norm())
        Att = 0.5*(Att.__add__(Att.T))
	Att = (Att*100).__add__(tt.eye(n[i],d=d[i]))
        fname = "cgtest/CGTT_random"+"_n"+str(n[i])+"_d"+str(d[i])+"_m"+str(maxiter[i])+"_eps"+str(epsilon[i])+".py"

    Att = Att.round(epsilon[i]*1e-2)
    res = tt.rand(n[i],d=d[i],r=2)
    res = res*(1/res.norm())
    u_0=None
    if case[i] == 'random':
        temp = tt.ones(n[i],d=d[i])
        res = tt.matvec(Att, temp)
        print (res - tt.matvec(Att,temp)).norm()
        res = res*(1/res.norm())
    
    f = open(fname,"w")
    f.write("import matplotlib.pyplot as plt")
    f.write("\n# "+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(maxiter[i])+"\n")
    f.close()
    
    print "ttgmres adapt=0"
    TTCG.TTCG((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], maxiter=maxiter[i],adapt=0,fname=fname)

    print "ttgmres adapt=1"
    TTCG.TTCG((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i],  maxiter=maxiter[i],adapt=1,fname=fname)

    f = open(fname,"a")
    f.write("plt.xlabel(\'iteration\')\nplt.ylabel(\'max TT-rank\')\nplt.title(\'"+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(maxiter[i])+" TTCG\')\nplt.legend()\nplt.savefig(\'"+str(fname)+".png\',dpi=300)\n+plt.show()\n")
    f.close()
