import sys
sys.path.append('../')

import numpy as np
import numpy.linalg as la
import scipy.linalg as sl
import scipy.sparse.linalg as ssl
import scipy.io as sio
import tt as tt
import tt.eigb
import resaTT.tools as rtt
import Benchmark.spinchain as spin
import Benchmark.Laplace as laplace
import time

'''
for testing the eigb algorithm

Input:
L: Dimension of Problem
n: Modesize
case: Distinct which testcase to use (convdiff does not work since it is unsymmetric)
Bond: gives Bonddimension for startsolution. needs to be high enough, so that solution can be found with maximal that bonddimension
epsilon: wanted accuracy for eigb. residualnorm can be very different since eigb has different creteria for stopping
maxIter: apperently we don't use it anymore

Output:
it prints the runtime, the eigenvalue R, max Bonddimension of einenvector Q and norm of residual AQ-QR
'''
def TTpyALStest(L,n,case,Bond,epsilon,maxIter):
    if case=="spin":
        A = spin.TTspinOpB(L)
    elif case == "laplace":
        A = laplace.Laplace(n,L)
    elif case == "ConvDiff":
        A = laplace.simpleConvDiff(n,L)
    else:
        print "not implemented!"

    r = [Bond] * (L+1); r[L] = 1; r[0] = 1
#    for i in range(1,L/2):
#        r[i]=min(pow(2,i),Bond)
#        r[L-i]=min(pow(2,i),Bond)
    vec=tt.rand([n]*L,d=L,r=r)

    start = time.time()
    Q,R=tt.eigb.eigb(A, vec, epsilon,rmax=Bond,verb=0)
    end =time.time()

    print "time",end-start
    print "R =",R
    print "rmax(Q)=",rtt.get_rmax(Q)
    test2 = tt.matvec(A,Q)-Q*R[0]
    print "test",test2.norm()

case = "spin"
n = 2
maxIter = 50
L=[8,10,12,14,16,18,20,22]
Bond=[100,100,150,150,250,250,500,800]

for i in range(7,8):
    #for Bond in [250,500,1000,2000]:
        for epsilon in [1e-6,1e-10,1e-12]:
            print "case",case," epsilon",epsilon," Bond",Bond[i]," L",L[i]
            TTpyALStest(L[i],n,case,Bond[i],epsilon,maxIter)
