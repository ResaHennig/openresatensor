import sys
sys.path.append('../')

import tt as tt
import scipy.io as sio
import Arnoldi
import ArnoldiTT
import Benchmark.spinchain as spin
import Benchmark.Laplace as laplace
import resaTT.tools as rtt

'''
For running Tests with Arnoldi and ArnoldiTT
'''

'''setup test case - this needs to be changed individualy'''
case = 'spin' #options are: 'spin','laplace','ConvDiff'
d = [8,10,12,14] #dimensions to look at
n = 2 #elements in one dimension
epsilon = 1e-10 #accuracy for Arnoldi
maxIter = 10 #number of iterations per restart
restarts = 100 #number of restarts
alpha=0.1 #constant for convection
conv=None #directions of convection (set None for Default)

'''automatic part - do not change anything here'''

for i in d:
    if case == 'spin':
        print "create TTspinOpB"
        Att = spin.TTspinOpB(i)
        Att = Att.round(epsilon*1e-2)
        vec = ''
        for j in range(0,i/2):
            vec = vec+str(1)+str(0)
        t = spin.spinvec_from_binary(i,vec)
        t_full = rtt.TT2Vec(t)
        print "read sparseMatrix spinOpB"
        A = sio.mmread('../spin/spin'+str(i)+'.mm').tocsr()
    
    if case == 'laplace':
        print "create TTlaplace"
        Att = laplace.Laplace(n,i)
        Att = Att.round(epsilon*1e-2)
        print "create sparse laplace"
        A = laplace.Laplace_sparse(n,i)
        t = tt.ones(n,d=i)
        t_full = rtt.TT2Vec(t)

    if case == 'ConvDiff':
        print "create TTConvDiff"
        Att = laplace.ConvDiff(n,i,conv=conv,alpha=alpha)
        Att = Att.round(epsilon*1e-2)
        print "create sparse ConvDiff"
        A = laplace.ConvDiff_sparse(n,i,conv=conv,alpha=alpha)
        t = tt.ones(n,d=i)
        t_full = rtt.TT2Vec(t)
        
    print "d",i,"case",case,"epsilon",epsilon,"alpha",alpha
    (QTT,RTT) = ArnoldiTT.Arnoldi(Att, t, epsilon, maxIter,restarts)
    print("RTT",RTT)
    print "test", (tt.matvec(Att,QTT)-QTT*RTT).norm()
    (Q,R)=Arnoldi.Arnoldi(A,t_full,epsilon,maxIter,restarts)
    print("R",R)
