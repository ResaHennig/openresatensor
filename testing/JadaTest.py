import sys
sys.path.append('../')

import tt as tt
import scipy.io as sio
import JacobiDavidson as Jada
import JacobiDavidsonTT as JadaTT
import Benchmark.spinchain as spin
import Benchmark.Laplace as laplace
import resaTT.tools as rtt

'''
For running Tests with Jada and JadaTT
'''

'''setup test case - this needs to be changed individualy'''
case = 'ConvDiff' #options are: 'spin','laplace','ConvDiff','ConvDiffx1'
subcase = 'fullTT' #if not 'sparse' or 'dense' it computes fullTT version
d = [6] #dimensions to look at
n = 4 #elements in one dimension
epsilon = 1e-8 #accuracy for Jada
maxIter = 1000 #maximal number of outer iterations
adapt = True #adaptive accuracy in gmres
gmres_tol = 1e-9 #accuracy of gmres, if adapt==True then accuracy = norm(res)*gmres_tol
arnoldiIter = 0 #number of arnoldi steps at the beginning of the algorithm
save = False #if True saves time information in a file (for measuring runtime)
alpha=0.1 #constant for convection
conv=None #[1] #directions of convection (set None for Default)

'''automatic part - do not change anything here'''
fullTT = False
symm = True

if save:
    f = open("JadaTT"+case+subcase+"_n"+str(n)+"_eps"+str(epsilon)+"_gmresadapt"+str(adapt)+str(gmres_tol)+"_arnoldi"+str(arnoldiIter)+"_alpha"+str(alpha)+"conv"+str(conv)+".txt","a")
    f.write("d\ttime\tgmres\touter\n")
    f.close()
    f = open("Jada"+case+subcase+"_n"+str(n)+"_eps"+str(epsilon)+"_gmresadapt"+str(adapt)+str(gmres_tol)+"_arnoldi"+str(arnoldiIter)+"_alpha"+str(alpha)+"conv"+str(conv)+".txt","a")
    f.write("d\ttime\tgmres\touter\n")
    f.close()

for i in d:
    if case == 'spin':
        print "create TTspinOpB"
        Att = spin.TTspinOpB(i)
        Att = Att.round(epsilon*1e-2)
        vec = ''
        for j in range(0,i/2):
            vec = vec+str(1)+str(0)
        t = spin.spinvec_from_binary(i,vec)
        t_full = rtt.TT2Vec(t)
        if subcase == 'sparse':
            print "read sparseMatrix spinOpB"
            A = sio.mmread('../spin/spin'+str(i)+'.mm').tocsr()
        elif subcase == 'dense':
            print "read denseMatrix spinOpB"
            A = sio.mmread('../spin/spin'+str(i)+'.mm').toarray()
        else:
            A = None
            fullTT = True
    
    if case == 'laplace':
        print "create TTlaplace"
        Att = laplace.Laplace(n,i)
        Att = Att.round(epsilon*1e-2)
        if subcase == 'dense':
            print "create dense laplace"
            A = Att.full()
        elif subcase == 'sparse':
            print "create sparse laplace"
            A = laplace.Laplace_sparse(n,i)
        else:
            A = None
            fullTT = True
        t = tt.ones(n,d=i)
        t_full = rtt.TT2Vec(t)

    if case == 'ConvDiff':
        print "create TTConvDiff"
        Att = laplace.ConvDiff(n,i,conv=conv,alpha=alpha)
        Att = Att.round(epsilon*1e-2)
        if subcase == 'dense':
            print "create dense ConvDiff"
            A = Att.full()
        elif subcase == 'sparse':
            print "create sparse ConvDiff"
            A = laplace.ConvDiff_sparse(n,i,conv=conv,alpha=alpha)
        else:
            A = None
            fullTT = True
        t = tt.ones(n,d=i)
        t_full = rtt.TT2Vec(t)
        symm=False
        
    if case == 'ConvDiffx1':
        print "create TTConvDiffx1"
        Att = laplace.ConvDiffx1(n,i,alpha=alpha)
        Att = Att.round(epsilon*1e-2)
        if subcase == 'dense':
            print "create dense ConvDiffx1"
            A = Att.full()
        elif subcase == 'sparse':
            print "create sparse ConvDiffx1"
            A = laplace.ConvDiffx1_sparse(n,i,alpha=alpha)
        else:
            A = None
            fullTT = True
        t = tt.ones(n,d=i)
        t_full = rtt.TT2Vec(t)
        symm=False

    if save:
        print "d",i,"case",case,"subcase",subcase,"epsilon",epsilon,"arnoldi",arnoldiIter,"alpha",alpha
        (QTT,RTT,runtimeTT,gmrestimeTT,outertimeTT) = JadaTT.JacobiDavidson(A,Att, t, epsilon, maxIter,gmres_tol=gmres_tol,adapt=adapt,fullTT=fullTT,symm=symm,save=save,arnoldiIter=arnoldiIter)
        f = open("JadaTT"+case+subcase+"_n"+str(n)+"_eps"+str(epsilon)+"_gmresadapt"+str(adapt)+str(gmres_tol)+"_arnoldi"+str(arnoldiIter)+"_alpha"+str(alpha)+"conv"+str(conv)+".txt","a")
        f.write(str(i)+"\t"+str(runtimeTT)+"\t"+str(gmrestimeTT)+"\t"+str(outertimeTT)+"\n")
        f.close()
        print("RTT",RTT)
        if not fullTT:
            (Q,R,runtime,gmrestime,outertime)=Jada.JacobiDavidson(A,t_full,epsilon,maxIter,gmres_tol=gmres_tol,adapt=adapt,symm=symm,save=save)
            f = open("Jada"+case+subcase+"_n"+str(n)+"_eps"+str(epsilon)+"_gmresadapt"+str(adapt)+str(gmres_tol)+"_arnoldi"+str(arnoldiIter)+"_alpha"+str(alpha)+"conv"+str(conv)+".txt","a")
            f.write(str(i)+"\t"+str(runtime)+"\t"+str(gmrestime)+"\t"+str(outertime)+"\n")
            f.close()
            print("R",R)
    else:
        print "d",i,"case",case,"subcase",subcase,"epsilon",epsilon,"arnoldi",arnoldiIter,"alpha",alpha
        (QTT,RTT) = JadaTT.JacobiDavidson(A,Att, t, epsilon, maxIter,gmres_tol=gmres_tol,adapt=adapt,fullTT=fullTT,symm=symm,save=save,arnoldiIter=arnoldiIter,verb=1)
        (QTT,RTT) = JadaTT.TTJacobiDavidson(Att, t, epsilon, maxIter,gmres_tol=gmres_tol,adapt=adapt,save=save,arnoldiIter=arnoldiIter,verb=1)
        print("RTT",RTT)
        #Qtt=rtt.Vec2TT(QTT,t.n)
        #print "test", (tt.matvec(Att,Qtt)-Qtt*RTT).norm()
        if not fullTT:
            (Q,R)=Jada.JacobiDavidson(A,t_full,epsilon,maxIter,gmres_tol=gmres_tol,adapt=adapt,symm=symm,save=save,verb=1)
            print("R",R)
