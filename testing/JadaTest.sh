#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH -t 20:00:00
#SBATCH --error=error.e

PYTHONUNBUFFERED=1 srun --cpu-bind=core python JadaTest.py
