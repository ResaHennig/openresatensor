import sys
sys.path.append('../')

import tt as tt
import scipy.io as sio
import resaTT.TTgmres5 as TTgmres5
import Benchmark.Laplace as laplace
import resaTT.tools as rtt
import JacobiDavidsonTT as TTJada
import time
import matplotlib.pyplot as plt
import numpy as np

def Test(A,x,eps=1e-14):
    sol=rtt.matvec(A,x)
    return rtt.round(sol,eps)

n = [20,20,20,20,20,20,20]
d = [8] #4,4,6,6,8,8]
restart = [80] #40,80,40,80,40,80]
case = ['CD','CD','La','CD','CD','CD','CD']
epsilon = [1e-8,1e-8,1e-8,1e-4,1e-8,1e-6] #1e-4,1e-8,1e-4,1e-8,1e-4,1e-8]

conv = None
alpha = 0.1
maxiter = 1
verb = 0

for i in range(len(d)):
    if case[i] == 'La':
        print "create Laplace n=",n[i],"d=",d[i]
        Att = laplace.Laplace(n[i],d[i])
        fname = "gmrestest/GMRESTT_Laplace"+"_n"+str(n[i])+"_d"+str(d[i])+"_m"+str(restart[i])+"_eps"+str(epsilon[i])+".py"
        plotname = "La_n"+str(n[i])+"_d"+str(d[i])+"_eps"+str(epsilon[i])+"_m"+str(restart[i])

    elif case[i] == 'CD':
        print "create TTConvDiff n=",n[i],"d=", d[i]
        Att = laplace.ConvDiff(n[i],d[i],conv=conv,alpha=alpha)
        fname = "gmrestest/GMRESTT_ConvDiffNone01"+"_n"+str(n[i])+"_d"+str(d[i])+"_m"+str(restart[i])+"_eps"+str(epsilon[i])+".py"
        plotname = "CDNone01_n"+str(n[i])+"_d"+str(d[i])+"_eps"+str(epsilon[i])+"_m"+str(restart[i])
    elif case[i] == 'random':
        print "create random operator n=",n[i],"d=",d[i]
        temp = tt.rand(n[i]*n[i],d=d[i],r=2)
        temp = temp*(1/temp.norm())
        #print temp.full()
        Att = tt.matrix(a=temp)
        Att = Att.__add__(Att.T)
        #print Att.full()
	Att = (0.5*Att*0.0001).__add__(tt.eye(n[i],d=d[i]))
        #print Att.full()
        fname = "gmrestest/GMRESTT_random"+"_n"+str(n[i])+"_d"+str(d[i])+"_m"+str(restart[i])+"_eps"+str(epsilon[i])+".py"
        plotname = "random_n"+str(n[i])+"_d"+str(d[i])+"_eps"+str(epsilon[i])+"_m"+str(restart[i])
    Att = Att.round(epsilon[i]*1e-2)
    res = tt.rand(n[i],d=d[i],r=2)
    res = res*(1/res.norm())
    u_0=None
    if case[i] == 'random':
        temp = tt.ones(n[i],d=d[i])
        res = tt.matvec(Att, temp)
        print 'rang rechte Seite',rtt.get_rmax(res)
        print (res - tt.matvec(Att,temp)).norm()
        print 'rang loesung',rtt.get_rmax(temp)
        print 'rang op', rtt.get_rmax(Att)
        res = res*(1/res.norm())
        #u_0 = tt.rand(n[i],d=d[i],r=1)
    
    f = open(fname,"w")
    f.write("import matplotlib.pyplot as plt")
    f.write("\n# "+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(restart[i])+"\n")
    f.close()

    print "ttgmres adapt=0 simgs"
    TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i], maxiter=maxiter,verb=verb,adapt=0,fname=fname,gsmod='sim')

    print "ttgmres adapt=1 simgs"
    TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i], maxiter=maxiter,verb=verb,adapt=1,fname=fname,gsmod='sim')

    f = open(fname,"a")
    f.write("plt.xlabel(\'iteration\')\nplt.ylabel(\'max TT-rank\')\nplt.title(\'"+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(restart[i])+" TTgmres ipmgs\')\nplt.legend()\nplt.savefig(\'"+str(plotname)+"_ipmgs.png\',dpi=300)\n#plt.show()\n")
    f.close()
    
    #print "ttgmres adapt=0 simgs restarted"
    #TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i]/2, maxiter=2,verb=verb,adapt=0,fname=fname,gsmod='sim')

    #print "ttgmres adapt=1 simgs restarted"
    #TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i]/2, maxiter=2,verb=verb,adapt=1,fname=fname,gsmod='sim')

    #f = open(fname,"a")
    #f.write("plt.xlabel(\'iteration\')\nplt.ylabel(\'max TT-rank\')\nplt.title(\'"+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(restart[i]/2)+" restarted TTgmres ipmgs\')\nplt.legend()\nplt.savefig(\'"+str(plotname)+"_restarted_ipmgs.png\',dpi=300)\nplt.show()\n")
    #f.close()
    
    print "ttgmres adapt=0 mgs"
    TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i], maxiter=maxiter,verb=verb,adapt=0,fname=fname,gsmod='m')

    print "ttgmres adapt=1 mgs"
    TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i], maxiter=maxiter,verb=verb,adapt=1,fname=fname,gsmod='m')

    f = open(fname,"a")
    f.write("plt.xlabel(\'iteration\')\nplt.ylabel(\'max TT-rank\')\nplt.title(\'"+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(restart[i])+" TTgmres mgs\')\nplt.legend()\nplt.savefig(\'"+str(plotname)+"_mgs.png\',dpi=300)\n#plt.show()\n")
    f.close()
    
    #print "ttgmres adapt=0 mgs restarted"
    #TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i]/2, maxiter=2,verb=verb,adapt=0,fname=fname,gsmod='m')

    #print "ttgmres adapt=1 mgs restarted"
    #TTgmres5.TTGMRES((lambda x,eps=1e-14:Test(Att,x,eps=eps)), res,u_0=u_0, eps=epsilon[i], restart=restart[i]/2, maxiter=2,verb=verb,adapt=1,fname=fname,gsmod='m')

    f = open(fname,"a")
    #f.write("plt.xlabel(\'iteration\')\nplt.ylabel(\'max TT-rank\')\nplt.title(\'"+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(restart[i]/2)+" restarted TTgmres mgs\')\nplt.legend()\nplt.savefig(\'"+str(plotname)+"_restarted_mgs.png\',dpi=300)\nplt.show()\n")
            
    f.write("\nfig = plt.figure()\nax = plt.gca()\nplt.semilogy(LSTSQ_res1,marker=\'o\',markerfacecolor=\'none\',markevery=5,label=\'nonadaptiv\')\nplt.semilogy(LSTSQ_res2,marker=\'x\',markevery=5,label=\'adaptiv\')\nplt.semilogy(LSTSQ_res3,marker=\'D\',markerfacecolor=\'none\',markevery=5,label=\'nonadaptiv\')\nplt.semilogy(LSTSQ_res4,marker=\'^\',markerfacecolor=\'none\',markevery=5,label=\'adaptiv\')\nplt.xlabel(\'iteration\')\nplt.ylabel(\'LSTSQ residual\')\nplt.title(\'"+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(restart[i])+" TTgmres ipmgs\')\nplt.legend()\nplt.savefig(\'"+str(plotname)+"_res_ipmgs.png\',dpi=300)\nplt.show()\n")
    
    f.write("\nfig = plt.figure()\nax = plt.gca()\nplt.semilogy(LSTSQ_res5,marker=\'o\',markerfacecolor=\'none\',markevery=5,label=\'nonadaptiv\')\nplt.semilogy(LSTSQ_res6,marker=\'x\',markevery=5,label=\'adaptiv\')\nplt.xlabel(\'iteration\')\nplt.semilogy(LSTSQ_res7,marker=\'D\',markerfacecolor=\'none\',markevery=5,label=\'nonadaptiv\')\nplt.semilogy(LSTSQ_res8,marker=\'^\',markerfacecolor=\'none\',markevery=5,label=\'adaptiv\')\nplt.ylabel(\'LSTSQ residual\')\nplt.title(\'"+str(case[i])+" n="+str(n[i])+" d="+str(d[i])+" eps="+str(epsilon[i])+" m="+str(restart[i])+" TTgmres mgs\')\nplt.legend()\nplt.savefig(\'"+str(plotname)+"_res_mgs.png\',dpi=300)\nplt.show()\n")

    f.close()
    
    
    

