import sys
sys.path.append('../')

import tt as tt
import scipy.io as sio
import JacobiDavidson as Jada
import JacobiDavidsonTT as JadaTT
import Benchmark.spinchain as spin
import Benchmark.Laplace as laplace
import resaTT.tools as rtt

'''
For running Tests with Jada and JadaTT
'''

def powerset(s):
    set=[]
    x=len(s)
    for i in range(1,1<<x):
        set.append([s[j] for j in range(x) if (i&(1<<j))])
    return set

'''setup test case - this needs to be changed individualy'''
subcase = 'sparse' #if not 'sparse' it computes fullTT version
d = 10 #dimensions to look at
n = 4 #elements in one dimension
epsilon = 1e-10 #accuracy for Jada
maxIter = 20 #maximal number of outer iterations
adapt = True #adaptive accuracy in gmres
gmres_tol = 1e-4 #accuracy of gmres, if adapt==True then accuracy = norm(res)*gmres_tol
arnoldiIter = 0 #number of arnoldi steps at the beginning of the algorithm
alpha = 0.1 #constant for convection

'''automatic part - do not change anything here'''
fullTT = False
symm = False

conv = powerset(range(1,d+1))

for i in range(len(conv)):
    print "create TTConvDiff"
    Att = laplace.ConvDiff(n,d,conv=conv[i],alpha=alpha)
    Att = Att.round(epsilon*1e-2)
    if subcase == 'sparse':
        print "create sparse ConvDiff"
        A = laplace.ConvDiff_sparse(n,d,conv=conv[i],alpha=alpha)
    else:
        A = None
        fullTT = True
    t = tt.ones(n,d=d)
    t_full = rtt.TT2Vec(t)

    print "d",d,"ConvDiff","subcase",subcase,"epsilon",epsilon,"arnoldi",arnoldiIter,"alpha",alpha,"conv",conv[i]
    (QTT,RTT) = JadaTT.JacobiDavidson(A,Att, t, epsilon, maxIter,gmres_tol=gmres_tol,adapt=adapt,fullTT=fullTT,symm=symm,arnoldiIter=arnoldiIter,verb=1)
    print("RTT",RTT)
