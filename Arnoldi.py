import numpy as np
import numpy.linalg as la
import scipy.linalg as sla
import resaTT.tools as rtt
import resatools as rt
import scipy.io as sio
import tt as tt
import time

def Arnoldi(A,t,epsilon,maxIter,restarts,verb=0):
	start=time.time()
	dim = len(t)
	
	v = t/la.norm(t)	
	for k in range(0,restarts):
		if verb>0:
			print "restart",k
		H = np.zeros((1,0))
		W = np.zeros((dim,0))
		nIter = maxIter
		for j in range(1,maxIter+1):
			W = np.append(W,v,axis=1)
			vA = A.dot(v)
			H = np.append(H,np.zeros((j,1)),axis=1)
			for i in range(1,j+1):
				H[i-1,j-1] = np.dot(rt.conjt(vA),W[:,i-1].reshape((dim,1)))[0,0]
			v = vA
			for i in range(1,j+1):
				v = v - H[i-1,j-1]*W[:,i-1].reshape((dim,1))
			H = np.append(H,np.zeros((1,j)),axis=0)
			H[j,j-1] = la.norm(v)
		
			if H[j,j-1]<1e-14:
				nIter = j
				break
			v = v/H[j,j-1]
	
		(RH,QH) = la.eig(H[0:nIter,0:nIter])
		ind = np.argmin(RH)
		R = RH[ind]
		Q = np.zeros((dim,1))
		for i in range(0,nIter):
			Q = Q + W[:,i].reshape((dim,1))*QH[i,ind]
		res = A.dot(Q)-Q*R
		normres = la.norm(res)
		if verb>0:
			print("normres "+str(normres))
		if normres<epsilon:
			ende = time.time()
			print("Arnoldi converged")
			print('zeit: {:5.3f}s'.format(ende-start))
			return (Q,R)
		v = Q/la.norm(Q)
	ende = time.time()
	print("Arnoldi not converged")
	print('zeit: {:5.3f}s'.format(ende-start))
	return (Q,R)
