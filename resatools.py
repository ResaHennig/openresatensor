import numpy as np

'''
computes the complex conjugate transposed of matrix A

this is just for shorter writing in code
'''
def conjt(A):
    return np.conjugate(A).transpose()
