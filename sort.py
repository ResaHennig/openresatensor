import numpy as np

'''
select certain Eigenpairs of a Schurdecomposition

Input:
R,Q: Schurdecomposition
nselect: number of wanted Eigenpairs
which: kind of selection, possible LM (largest magnitude) or SM (smallest magnitude)

Output:
the <nselect> largest/smallest magnitude Eigenpairs 
'''
def selectEigen(R,Q,nselect,which):
	(n1Q,n2Q) = Q.shape
	
	if nselect>n2Q:
		print "Error: Cannot select more eigenpairs than given to the routine"
		return 0	
	
	EW = np.zeros((nselect))
	EV = np.zeros((n1Q,nselect))
	
	tosort = np.absolute(np.diag(R))
	sorting = np.argsort(tosort)
	
	if which=='LM':
		for i in range(0,nselect):
			EW[i]=R[sorting[n1Q-i-1],sorting[n1Q-i-1]]
			EV[:,i]=Q[:,sorting[n1Q-i-1]]
	
	elif which=='SM':
		for i in range(0,nselect):
			EW[i]=R[sorting[i],sorting[i]]
			EV[:,i]=Q[:,sorting[i]]
	
	elif which=='None':
		for i in range(0,nselect):
			EW[i]=R[i,i]
			EV[:,i]=Q[:,i]
			
	else:
		print "Parameter which not set correctly. It only allows 'LM','SM','None'"
		return 0
	
	return (EW,EV)
